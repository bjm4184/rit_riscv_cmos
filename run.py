
from os.path import join, dirname
from vunit import VUnit

root = dirname(__file__)
src = join(root, "src")
tb = join(root, "tb")

ui = VUnit.from_argv()
lib = ui.add_library("lib")

# Add all of the source files
lib.add_source_files(join(src, "misc/*.vhd"))
lib.add_source_files(join(src, "datapath/*.vhd"))
lib.add_source_files(join(src, "mem/*.vhd"))
lib.add_source_files(join(tb, "datapath/*.vhd"))
lib.add_source_files(join(tb, "mem/*.vhd"))

ui.main()
