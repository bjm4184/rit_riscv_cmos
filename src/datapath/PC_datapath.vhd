--  PC_datapath.vhd
--  9/23/17
--  Braeden Morrison
--
--  The implementation of the portion of the datapath 
--      around the program counter.
--  This implementation will contain the program counter, a 4-to-1 mux, 
--      and a couple of sign-extenders and adders.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity PC_datapath is
    generic (
        --=====================================================================
        -- WIDTH:           The width of the datapath and registers
        -- BRANCH_IMM_BITS: The size of the immediate for a branch
        -- JAL_IMM_BITS:    The size of the immediate for a JAL
        -- JALR_IMM_BITS:   The size of the immediate for a JALR
        --=====================================================================

        WIDTH :             natural := 32; 
        BRANCH_IMM_BITS :   natural := 12;
        JAL_IMM_BITS :      natural := 20;
        JALR_IMM_BITS :     natural := 12
    );
    port (
        --=====================================================================
        ---- Input Signals ----

        -- branch_imm:  The signed immediate value used to calculate the branch
        --                  destination. Comes from instruction decode.
        -- jal_imm:     The immediate value that will be sign extended and used
        --                  to calculate the destination for jump. Comes from 
        --                  instruction decode.
        -- jalr_imm:    The signed immediate value that gets added to the
        --                  'jalr_reg' value to determine jump destination.
        --                  Comes from instruction decode.
        -- jalr_reg:    The value from the register file that gets added to
        --                  'jalr_imm' to determine the destination for JALRs.
        --                  Comes from the register-file.

        -- jmp_ctrl:    A two-bit signal used to select the next value for PC.
        --                  Comes from the control unit.

        -- clk:         A one-bit clock signal that gets sent to the PC. Comes
        --                  from the clock circuitry.
        -- pc_rst_n:    A one-bit active-low reset signal for the PC. I'm not
        --                  really sure where this one comes from, but it could
        --                  even be from the power-supply or something.

        ---- Output Signals ----

        -- cur_pc:      The current value in the PC register. Goes to the
        --                  register-file datapath.
        -- next_pc:     The next value for the PC assuming to jumps or branches
        --                  (Is equal to cur_pc + 4). Goes to the resgiter-file
        --                  datapath.
        --=====================================================================

        branch_imm :     in  std_logic_vector(BRANCH_IMM_BITS-1 downto 0);
        jal_imm :        in  std_logic_vector(JAL_IMM_BITS-1 downto 0);
        jalr_imm :       in  std_logic_vector(JALR_IMM_BITS-1 downto 0);
        jalr_reg :       in  std_logic_vector(WIDTH-1 downto 0);

        jmp_ctrl :       in  std_logic_vector(1 downto 0);

        clk :            in  std_logic;
        pc_rst_n :       in  std_logic;
    
        cur_pc :         out std_logic_vector(WIDTH-1 downto 0); 
        next_pc :        out std_logic_vector(WIDTH-1 downto 0)
    );
end PC_datapath;

architecture stub of PC_datapath is
begin

    cur_pc  <= (others => 'X');
    next_pc <= (others => 'X');

end stub;
