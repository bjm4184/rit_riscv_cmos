--  alu_datapath.vhd
--  10/5/17
--  Braeden Morrison
--
--  The portion of the datapath around the ALU.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity alu_datapath is
    generic (
        --=================================================================
        -- WIDTH:           The word size for the processor (32 bits).
        -- OP_BITS:         The number of bits in the ALU opcode.
        -- IMM_BITS:        The size of the immediate operand.
        --=================================================================
        WIDTH :             natural := 32;
        IMM_BITS :          natural := 12;
        OP_BITS :           natural := 3
    );
    port (
        --=================================================================
        ---- Input Signals ----

        -- reg_in1:     The first input from the register file. Serves as
        --                  the first operand and connects to rs1_out from
        --                  the reg file.
        -- reg_in2:     The second input from the register file. Serves as
        --                  the second operand in register-register 
        --                  operations. Comes from rs2_out of the reg file.
        -- imm_in:      The immediate used in register-immediate 
        --                  operations. Comes from instruction decode.
        --
        -- alu_in2_sel: The control signal that determines whether the 
        --                  operand to the ALU is an immediate or register
        --                  value. Comes from the control unit.
        -- alu_ctrl:    The control signal that selects between both 
        --                  addition and subtraction and between arithmetic
        --                  and logical right shifting. Comes from the 
        --                  control unit.
        -- alu_op:      The opcode for the ALU. Determines which operation
        --                  is performed. Comes from the control unit.

        ---- Output Signals ----

        -- alu_out:     The primary output of the ALU. Goes to the reg file
        --                  and the memory datapaths.
        -- compare_out: The secondary output of the ALU. Indicates whether
        --                  or not a branch should be taken during a branch
        --                  instruction.
        --=================================================================

        reg_in1 :       in  std_logic_vector(WIDTH-1 downto 0);
        reg_in2 :       in  std_logic_vector(WIDTH-1 downto 0);
        imm_in :        in  std_logic_vector(IMM_BITS-1 downto 0);

        alu_in2_sel :   in  std_logic;
        alu_ctrl :      in  std_logic;
        alu_op :        in  std_logic_vector(OP_BITS-1 downto 0);

        alu_out :       out std_logic_vector(WIDTH-1 downto 0);
        compare_out :   out std_logic
    );
end alu_datapath;

architecture stub of alu_datapath is
begin

    alu_out <= (others => 'X');
    compare_out <= 'X';

end stub;
