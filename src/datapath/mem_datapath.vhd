--  mem_datapath.vhd
--  9/25/17
--  Braeden Morrison
--
--  The portion of the datapath that contains the memory controller and its
--      surrounding circuitry, including muxes and extenders.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity mem_datapath is
    generic (
        --=====================================================================
        -- WIDTH:   The word size for the processor (32 bits).
        --=====================================================================
        WIDTH :     natural := 32
    );
    port (
        --=====================================================================
        ---- Input Signals ----

        -- data_in:     The data that goes to memory during a write. Connects
        --                  to the rs2 output of the register file.
        -- addr_in:     The address that will be read from or written to. Comes
        --                  from the output of the ALU.
        --
        -- mem_rw:      Controls whether we read from or write to memory. Comes
        --                  from the control unit.
        -- mem_bytes:   Determines how many bytes to read or write to memory.
        --                  Comes from the control unit.
        -- sign_ext:    Determines whether the output is zero or sign extended.
        --                  Comes from the control unit.
        --
        -- clk:         A one-bit clock signal that gets sent to the PC. Comes
        --                  from the clock-generating circuitry.
    
        ---- Output Signals ----

        -- mem_out:     The output from the memory unit. Goes to the ALU
        --                  potion of the datapath.
        --=====================================================================

        data_in :       in  std_logic_vector(WIDTH-1 downto 0);
        addr_in :       in  std_logic_vector(WIDTH-1 downto 0);
        mem_rw :        in  std_logic := '0';
        mem_bytes :     in  std_logic_vector(1 downto 0);
        sign_ext :      in  std_logic := '0';
        clk :           in  std_logic := '0';

        mem_out :       out std_logic_vector(WIDTH-1 downto 0)

    );
end mem_datapath;

architecture stub of mem_datapath is
begin

    mem_out <= (others => 'X');

end stub;
