--  regfile_datapath.vhd
--  9/27/17
--  Braeden Morrison
--
--  The portion of the datapath around the register file.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity regfile_datapath is
    generic (
        --=================================================================
        -- WIDTH:           The word size for the processor (32 bits).
        -- SEL_BITS:        Number of bits needed to specify a register.
        -- AUIPC_IMM_BITS:  Size of the immediate for an AUIPC.
        -- LUI_IMM_BITS:    Size of the immediate for a LUI.
        --=================================================================
        WIDTH :             natural := 32;
        SEL_BITS :          natural := 5;
        AUIPC_IMM_BITS :    natural := 20;
        LUI_IMM_BITS :      natural := 20
    );
    port (
        --=================================================================
        ---- Input Signals ----

        -- pc_in:       The value output by the PC. Used in AUIPC, JAL, and
        --                  JALR instructions. Comes from the PC datapath.
        -- alu_in:      The result of the ALU operation. Comes from the ALU
        --                  datapath.
        -- mem_in:      The output from the memory controller; representing
        --                  a value read from memory. Comes from the mem
        --                  datapath.
        --
        -- auipc_imm:   The unshifted immediate value that will eventually
        --                  be added to the PC in a AUIPC instruction.
        --                  comes from the instruction decode.
        -- lui_imm:     The unshifted immediate value that will be used in
        --                  an LUI instruction. Comes from the instruction
        --                  decode.
        --
        -- rwrite_en:   Controls whether or not a value is written to the
        --                  register file. Comes from the control unit.
        -- reg_in_ctrl: Determines which value will be stored into the 
        --                  register file. Comes from the control unit.
        -- res_ctrl:    Determines whether the output from the ALU or from
        --                  memory will go to the register file. Comes from
        --                  the control unit.
        --
        -- clk:         A one-bit clock signal that gets sent to the 
        --                  register file. Comes from the clock-generating
        --                  circuitry.
        -- rs1_sel:     The signal that selects which register will be used
        --                  as rs1. Comes from the control unit.
        -- rs2_sel:     The signal that selects which register will be used
        --                  as rs2. Comes from the control unit.
        -- rd_sel:      The signal that selects which register will be used
        --                  as rd. Comes from the control unit.

        ---- Output Signals ----

        -- rs1_out:     The output from the first source register. Goes
        --                  to the PC and ALU datapaths.
        -- rs2_out:     The output from the second source register. Goes
        --                  to the mem and ALU datapaths.
        --=================================================================

        pc_in :         in  std_logic_vector(WIDTH-1 downto 0);
        alu_in :        in  std_logic_vector(WIDTH-1 downto 0);
        mem_in :        in  std_logic_vector(WIDTH-1 downto 0);
        auipc_imm :     in  std_logic_vector(AUIPC_IMM_BITS-1 downto 0);
        lui_imm :       in  std_logic_vector(LUI_IMM_BITS-1 downto 0);
        rwrite_en :     in  std_logic;
        reg_in_ctrl :   in  std_logic_vector(1 downto 0);
        res_ctrl :      in  std_logic;
        clk :           in  std_logic;
        rs1_sel :       in  std_logic_vector(SEL_BITS-1 downto 0);
        rs2_sel :       in  std_logic_vector(SEL_BITS-1 downto 0);
        rd_sel :        in  std_logic_vector(SEL_BITS-1 downto 0);

        rs1_out :       out std_logic_vector(WIDTH-1 downto 0);
        rs2_out :       out std_logic_vector(WIDTH-1 downto 0)
    );
end regfile_datapath;

architecture stub of regfile_datapath is
begin

    rs1_out <= (others => 'X');
    rs2_out <= (others => 'X');

end stub;
