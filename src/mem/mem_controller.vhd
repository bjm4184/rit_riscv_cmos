
--  mem_controller.vhd
--  10/13/17
--  Braeden Morrison
--
--  A stub for memory controller. The initial implementation will probably only
--   implement 256 bytes of storage and have the remaining bytes be "reserved"
--   since this isn't a real memory controller, but just a place-holder.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity mem_controller_tb is
    generic (
        --=================================================================
        -- WIDTH:   The word size for the processor (32 bits).
        --=================================================================
        WIDTH :     natural := 32
    );
    port (
        --=================================================================
        ---- Input Signals ----

        -- data_in:     The data that goes to memory during a write. 
        --                  Connects to the rs2 output of the register 
        --                  file.
        -- addr_in:     The address that will be read from or written to. 
        --                  Comes from the output of the ALU.
        --
        -- mem_rw:      Controls whether we read from or write to memory. 
        --                  Comes from the control unit.
        -- mem_bytes:   Determines how many bytes to read or write to 
        --                  memory. Comes from the control unit.
        --
        -- clk:         A one-bit clock signal that gets sent to the PC. 
        --                  Comes from the clock-generating circuitry.

        ---- Output Signals ----

        -- word_out:    A 32-bit output from the memory used when reading
        --                  full words. Goes to the datapath.
        -- hword_out:   A 16-bit output from the memory used when reading
        --                  half words. Goes to the datapath.
        -- byte_out:    A 8-bit output from the memory used when reading
        --                  individual bytes. Goes to the datapath.
        --=================================================================

        data_in :       in  std_logic_vector(WIDTH-1 downto 0);
        addr_in :       in  std_logic_vector(WIDTH-1 downto 0);
        mem_rw :        in  std_logic;
        mem_bytes :     in  std_logic_vector(1 downto 0);
        clk :           in  std_logic;

        -- BJM[NOTE]: It might be better to have just a single 4-bit output
        --              from the memory controller and handle the sign and
        --              zero extending handled within the memory controller
        --              instead of letting the datapath handle it.

        word_out :      out std_logic_vector(WIDTH-1 downto 0);
        hword_out :     out std_logic_vector((WIDTH/2)-1 downto 0);
        byte_out :      out std_logic_vector(7 downto 0)
    );
end mem_controller_tb;
