--  mux16.vhd
--  9/23/17
--  Braeden Morrison
--
--  A simple sixteen-to-one mux.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity mux16 is
    generic (
        WIDTH :         natural := 32
    );
    port (
        ---- Inputs ----

        in0 :           in  std_logic_vector(WIDTH-1 downto 0);
        in1 :           in  std_logic_vector(WIDTH-1 downto 0);
        in2 :           in  std_logic_vector(WIDTH-1 downto 0);
        in3 :           in  std_logic_vector(WIDTH-1 downto 0);
        in4 :           in  std_logic_vector(WIDTH-1 downto 0);
        in5 :           in  std_logic_vector(WIDTH-1 downto 0);
        in6 :           in  std_logic_vector(WIDTH-1 downto 0);
        in7 :           in  std_logic_vector(WIDTH-1 downto 0);
        in8 :           in  std_logic_vector(WIDTH-1 downto 0);
        in9 :           in  std_logic_vector(WIDTH-1 downto 0);
        in10 :          in  std_logic_vector(WIDTH-1 downto 0);
        in11 :          in  std_logic_vector(WIDTH-1 downto 0);
        in12 :          in  std_logic_vector(WIDTH-1 downto 0);
        in13 :          in  std_logic_vector(WIDTH-1 downto 0);
        in14 :          in  std_logic_vector(WIDTH-1 downto 0);
        in15 :          in  std_logic_vector(WIDTH-1 downto 0);

        sel :           in  std_logic_vector(3 downto 0);

        ---- Outputs ----

        mux_out :       out std_logic_vector(WIDTH-1 downto 0)
    );
end mux16;

architecture Behav of mux16 is
begin
    
    with sel select
        mux_out <=  in0  when "0000",
                    in1  when "0001",
                    in2  when "0010",
                    in3  when "0011",
                    in4  when "0100",
                    in5  when "0101",
                    in6  when "0110",
                    in7  when "0111",
                    in8  when "1000",
                    in9  when "1001",
                    in10 when "1010",
                    in11 when "1011",
                    in12 when "1100",
                    in13 when "1101",
                    in14 when "1110",
                    in15 when others;

end Behav;
