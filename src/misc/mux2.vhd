--  mux2.vhd
--  9/12/17
--  Braeden Morrison
--
--  A simple two-to-one mux.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity mux2 is
    generic (
        WIDTH :         natural := 32
    );
    port (
        ---- Inputs ----

        in0, in1 :      in  std_logic_vector(WIDTH-1 downto 0);
        sel :           in  std_logic;

        ---- Outputs ----

        mux_out :       out std_logic_vector(WIDTH-1 downto 0)
    );
end mux2;

architecture Behav of mux2 is
begin
    
    with sel select
        mux_out <=  in0 when '0',
                    in1 when others;

end Behav;
