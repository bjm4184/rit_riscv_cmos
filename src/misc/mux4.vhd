--  mux4.vhd
--  9/23/17
--  Braeden Morrison
--
--  A simple four-to-one mux.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity mux4 is
    generic (
        WIDTH :         natural := 32
    );
    port (
        ---- Inputs ----

        in0 :           in  std_logic_vector(WIDTH-1 downto 0);
        in1 :           in  std_logic_vector(WIDTH-1 downto 0);
        in2 :           in  std_logic_vector(WIDTH-1 downto 0);
        in3 :           in  std_logic_vector(WIDTH-1 downto 0);

        sel :           in  std_logic_vector(1 downto 0);

        ---- Outputs ----

        mux_out :       out std_logic_vector(WIDTH-1 downto 0)
    );
end mux4;

architecture Behav of mux4 is
begin
    
    with sel select
        mux_out <=  in0 when "00",
                    in1 when "01",
                    in2 when "10",
                    in3 when others;

end Behav;
