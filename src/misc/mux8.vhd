--  mux8.vhd
--  9/23/17
--  Braeden Morrison
--
--  A simple eight-to-one mux.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity mux8 is
    generic (
        WIDTH :         natural := 32
    );
    port (
        ---- Inputs ----

        in0 :           in  std_logic_vector(WIDTH-1 downto 0);
        in1 :           in  std_logic_vector(WIDTH-1 downto 0);
        in2 :           in  std_logic_vector(WIDTH-1 downto 0);
        in3 :           in  std_logic_vector(WIDTH-1 downto 0);
        in4 :           in  std_logic_vector(WIDTH-1 downto 0);
        in5 :           in  std_logic_vector(WIDTH-1 downto 0);
        in6 :           in  std_logic_vector(WIDTH-1 downto 0);
        in7 :           in  std_logic_vector(WIDTH-1 downto 0);

        sel :           in  std_logic_vector(2 downto 0);

        ---- Outputs ----

        mux_out :       out std_logic_vector(WIDTH-1 downto 0)
    );
end mux8;

architecture Behav of mux8 is
begin
    
    with sel select
        mux_out <=  in0 when "000",
                    in1 when "001",
                    in2 when "010",
                    in3 when "011",
                    in4 when "100",
                    in5 when "101",
                    in6 when "110",
                    in7 when others;

end Behav;
