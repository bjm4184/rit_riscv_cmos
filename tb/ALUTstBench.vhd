-- ALUTstBench.vhd
-- 10/8/17
-- Andrew Eberhard
--
-- Testbench testing functionality of entire ALU.
--==========================================================================
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.pkgConstants.ALL;

LIBRARY vunit_lib;
CONTEXT vunit_lib.vunit_context;

ENTITY ALU_tstBench IS
    generic (runner_cfg : string);
END ALU_tstBench;

ARCHITECTURE ALU_tst OF ALU_tstBench IS

    --=====================================================================
    -- Component Declaration-----------------------------------------------
    COMPONENT RISCV_ALU
        PORT (

            --==============================================================
            --
            --                      -- INPUTS --
            --
            --  FUNC_SEL:   Control bit used for selecting addition and
            --              subtraction in adder. Also used to select the 
            --              type of right shift within the shift unit.
            --
            --  ALU_OP:     Opcode controlling the output of the ALU.
            --              "OP_WIDTH" bits wide.
            --
            --  CMP_OP:     Opcode select for the branch control multiplexer
            --              within the compare unit.
            --
            --  OpA:        First input operand. "WIDTH" bits wide.
            --
            --  OpB:        Second input operand. "WIDTH" bits wide.
            --
            --                      -- OUTPUTS --
            --
            --  N:          Status flag indicating if adder result is 
            --              is negative.
            --
            --  C:          Status flag indicating if a carry exists from
            --              the completed operation.
            --
            --  V:          Status flag indicating if overflowed occurred.
            --  
            --  Z:          Status flag indicating if result is zero.
            --
            --  C_OUT:      Carry out of the adder.
            --  
            --  B_CNTRL:    1-bit branch control output.
            --
            --  ALU_OUT:    Result output of adder, logic, and shift units.
            --              "WIDTH" bits wide.
            --
            --==============================================================

            FUNC_SEL    :IN     STD_LOGIC;        
            ALU_OP      :IN     STD_LOGIC_VECTOR((OP_WIDTH - 1) DOWNTO 0);
            CMP_OP      :IN     STD_LOGIC_VECTOR((OP_WIDTH - 1) DOWNTO 0);
            OpA         :IN     STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
            OpB         :IN     STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);

            N           :OUT    STD_LOGIC;
            C           :OUT    STD_LOGIC;
            V           :OUT    STD_LOGIC;
            Z           :OUT    STD_LOGIC;
            C_OUT       :OUT    STD_LOGIC;
            B_CNTRL     :OUT    STD_LOGIC;
            ALU_OUT     :OUT    STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0)
        );
    END COMPONENT;
    
    --======================================================================
    -- Control Signals --
    SIGNAL  testCNT         :NATURAL    := 0;
    --======================================================================
    -- TEST SIGNALS --------------------------------------------------------
        
        -- INPUT SIGNALS --
    SIGNAL  FUNC_SEL_tb     :STD_LOGIC;
    SIGNAL  ALU_OP_tb       :STD_LOGIC_VECTOR ((OP_WIDTH - 1) DOWNTO 0);
    SIGNAL  CMP_OP_tb       :STD_LOGIC_VECTOR ((OP_WIDTH - 1) DOWNTO 0);
    SIGNAL  OpA_tb          :STD_LOGIC_VECTOR ((WIDTH - 1) DOWNTO 0);
    SIGNAL  OpB_tb          :STD_LOGIC_VECTOR ((WIDTH - 1) DOWNTO 0);

        -- OUTPUT SIGNALS --
    SIGNAL  N_tb            :STD_LOGIC;
    SIGNAL  C_tb            :STD_LOGIC;
    SIGNAL  V_tb            :STD_LOGIC;
    SIGNAL  Z_tb            :STD_LOGIC;
    SIGNAL  C_OUT           :STD_LOGIC;
    SIGNAL  B_CNTRL         :STD_LOGIC;
    SIGNAL  ALU_OUT         :STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);

    --======================================================================

BEGIN

-- Instantiate component under test
uut:    RISCV_ALU
        PORT MAP (
                    FUNC_SEL        =>  FUNC_SEL_tb,
                    ALU_OP          =>  ALU_OP_tb,
                    CMP_OP          =>  CMP_OP_tb,
                    OpA             =>  OpA_tb,
                    OpB             =>  OpB_tb,
                    N               =>  N_tb,
                    C               =>  C_tb,
                    V               =>  V_tb,
                    Z               =>  Z_tb,
                    C_OUT           =>  C_OUT,
                    B_CNTRL         =>  B_CNTRL,
                    ALU_OUT         =>  ALU_OUT
                );
tb: PROCESS

    BEGIN

	test_runner_setup(runner, runner_cfg);  -- Start Vunit
        
        --==================================================================
        -- TEST 1: Sub  w/ Sum = 0x00000000; BEQ  = '1' --
        testCNT <= testCNT + 1;
        
        FUNC_SEL_tb     <= '1';         -- Subtraction Control
        ALU_OP_tb       <= "000";       -- Add out   
        CMP_OP_tb       <= "111";       -- BEQ
        OpA_tb          <= x"00000000";
        OpB_tb          <= x"00000000";

        WAIT FOR PERIOD;
        ASSERT(     N_tb    = '0'
                AND C_tb    = '0'
                AND V_tb    = '0'
                AND Z_tb    = '1'
                AND B_CNTRL = '1'
                AND C_OUT   = '0'
                AND ALU_OUT = x"00000000"
            )
        REPORT "Error in output of ALU on test " 
                & natural'image(testCNT) & ".";
                                
        --==================================================================
        -- TEST 2: Sub w/ Carry Out, Zero; BGE = '1' --
        testCNT <= testCNT + 1;
        
        FUNC_SEL_tb     <= '1';         -- Addition Control
        ALU_OP_tb       <= "000";       -- Add out   
        CMP_OP_tb       <= "101";       -- BGE
        OpA_tb          <= x"A0000000";
        OpB_tb          <= x"A0000000";

        WAIT FOR PERIOD;
        ASSERT(     N_tb    = '0'
                AND C_tb    = '1'
                AND V_tb    = '0'
                AND Z_tb    = '1'
                AND B_CNTRL = '1'
                AND C_OUT   = '1'
                AND ALU_OUT = x"FFFFFFFF"
            )
        REPORT "Error in output of ALU on test " 
                & natural'image(testCNT) & "." SEVERITY ERROR;
        
        --==================================================================
        -- TEST 3: Sub w/ Negative, Carry Out; BLT = '1' --
        testCNT <= testCNT + 1;
        
        FUNC_SEL_tb     <= '1';         -- Subtraction Control
        ALU_OP_tb       <= "000";       -- Add out   
        CMP_OP_tb       <= "100";       -- BLT
        OpA_tb          <= x"00000000";
        OpB_tb          <= x"00000001";

        WAIT FOR PERIOD;
        ASSERT(     N_tb    = '1'
                AND C_tb    = '0'
                AND V_tb    = '0'
                AND Z_tb    = '0'
                AND B_CNTRL = '1'
                AND C_OUT   = '0'
                AND ALU_OUT = x"80000000"
            )
        REPORT "Error in output of ALU on test " 
                & natural'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 4: Left Shift; BGEU = '1' --
        testCNT <= testCNT + 1;
        
        FUNC_SEL_tb     <= '1';         -- Subtraction Control for compare
        ALU_OP_tb       <= "001";       -- Left Shift   
        CMP_OP_tb       <= "010";       -- BGEU
        OpA_tb          <= x"AAAAAAAA";
        OpB_tb          <= x"00000001";

        WAIT FOR PERIOD;
        ASSERT(     N_tb    = '1'
                AND C_tb    = '0'
                AND V_tb    = '0'
                AND Z_tb    = '0'
                AND B_CNTRL = '1'
                AND C_OUT   = '0'
                AND ALU_OUT = x"55555554"
            )
        REPORT "Error in output of ALU on test " 
                & natural'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 5: SLT; Negative; BGE = '1' --
        testCNT <= testCNT + 1;
        
        FUNC_SEL_tb     <= '1';         -- Subtraction Control for compare
        ALU_OP_tb       <= "010";       -- SLT
        CMP_OP_tb       <= "101";       -- BGE
        OpA_tb          <= x"00001234";
        OpB_tb          <= x"00012345";

        WAIT FOR PERIOD;
        ASSERT(     N_tb    = '1'
                AND C_tb    = '0'
                AND V_tb    = '0'
                AND Z_tb    = '0'
                AND B_CNTRL = '1'
                AND C_OUT   = '0'
                AND ALU_OUT = x"00000001"
            )
        REPORT "Error in output of ALU on test " 
                & natural'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 6: SLTU; Negative and Carry; BLTU = '1' --
        testCNT <= testCNT + 1;
        
        FUNC_SEL_tb     <= '1';         -- Subtraction Control for compare
        ALU_OP_tb       <= "011";       -- SLTU
        CMP_OP_tb       <= "011";       -- BLTU
        OpA_tb          <= x"76543210";
        OpB_tb          <= x"87654321";

        WAIT FOR PERIOD;
        ASSERT(     N_tb    = '1'
                AND C_tb    = '1'
                AND V_tb    = '0'
                AND Z_tb    = '0'
                AND B_CNTRL = '1'
                AND C_OUT   = '0'
                AND ALU_OUT = x"00000001"
            )
        REPORT "Error in output of ALU on test " 
                & natural'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 7: XOR; BNE = '1' --
        testCNT <= testCNT + 1;
        
        FUNC_SEL_tb     <= '1';         -- Subtraction Control for compare
        ALU_OP_tb       <= "100";       -- XOR
        CMP_OP_tb       <= "110";       -- BNE
        OpA_tb          <= x"AAAAAAAA";
        OpB_tb          <= x"55555555";

        WAIT FOR PERIOD;
        ASSERT(     N_tb    = '0'
                AND C_tb    = '0'
                AND V_tb    = '0'
                AND Z_tb    = '0'
                AND B_CNTRL = '1'
                AND C_OUT   = '0'
                AND ALU_OUT = x"FFFFFFFF"
            )
        REPORT "Error in output of ALU on test " 
                & natural'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 8: Logical Right Shift --
        testCNT <= testCNT + 1;
        
        FUNC_SEL_tb     <= '0';         -- Shift control for logical
        ALU_OP_tb       <= "101";       -- Right Shift
        CMP_OP_tb       <= "000";       -- No Branch Control
        OpA_tb          <= x"EEEEEEEE";
        OpB_tb          <= x"00000001";

        WAIT FOR PERIOD;
        ASSERT(     N_tb    = '0'
                AND C_tb    = '0'
                AND V_tb    = '0'
                AND Z_tb    = '0'
                AND B_CNTRL = '0'
                AND C_OUT   = '0'
                AND ALU_OUT = x"77777777"
            )
        REPORT "Error in output of ALU on test " 
                & natural'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 9: Arithmetic Right Shift--
        testCNT <= testCNT + 1;
        
        FUNC_SEL_tb     <= '1';         -- Shift control for arithmetic
        ALU_OP_tb       <= "101";       -- Right Shift
        CMP_OP_tb       <= "000";       -- No Branch Control
        OpA_tb          <= x"44444444";
        OpB_tb          <= x"00000001";

        WAIT FOR PERIOD;
        ASSERT(     N_tb    = '0'
                AND C_tb    = '0'
                AND V_tb    = '0'
                AND Z_tb    = '0'
                AND B_CNTRL = '0'
                AND C_OUT   = '0'
                AND ALU_OUT = x"88888888"
            )
        REPORT "Error in output of ALU on test " 
                & natural'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 10: OR; Negative; BLTU = '1'--
        testCNT <= testCNT + 1;
        
        FUNC_SEL_tb     <= '1';         -- Subtraction Control
        ALU_OP_tb       <= "110";       -- Bitwise OR
        CMP_OP_tb       <= "011";       -- BLTU
        OpA_tb          <= x"00000000";
        OpB_tb          <= x"FFFFFFFF";

        WAIT FOR PERIOD;
        ASSERT(     N_tb    = '1'
                AND C_tb    = '0'
                AND V_tb    = '0'
                AND Z_tb    = '0'
                AND B_CNTRL = '1'
                AND C_OUT   = '0'
                AND ALU_OUT = x"FFFFFFFF"
            )
        REPORT "Error in output of ALU on test " 
                & natural'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 11: AND; Carry, Overflow; BNE = '1'--
        testCNT <= testCNT + 1;
        
        FUNC_SEL_tb     <= '1';         -- Subtraction Control
        ALU_OP_tb       <= "111";       -- Bitwise AND
        CMP_OP_tb       <= "110";       -- BNE
        OpA_tb          <= x"00000000";
        OpB_tb          <= x"FFFFFFFF";

        WAIT FOR PERIOD;
        ASSERT(     N_tb    = '0'
                AND C_tb    = '1'
                AND V_tb    = '1'
                AND Z_tb    = '0'
                AND B_CNTRL = '1'
                AND C_OUT   = '0'
                AND ALU_OUT = x"00000000"
            )
        REPORT "Error in output of ALU on test " 
                & natural'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        WAIT; --SUSPEND INDEFINITELY

	test_runner_cleanup(runner);    -- Stop testbench

    END PROCESS;
END;

