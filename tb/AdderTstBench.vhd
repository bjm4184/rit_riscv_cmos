--  AdderTstBench.vhd
--  10/5/17
--  Andrew Eberhard
--
--  A testbench for 32-bit adder implementation.
----------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.pkgConstants.ALL;

LIBRARY vunit_lib;
CONTEXT vunit_lib.vunit_context;

ENTITY adder_tb IS
    generic (runner_cfg : string);
END adder_tb;

ARCHITECTURE tb_32bit_adder OF adder_tb IS
    
     --=====================================================================
    --- Component Declaration ----------------------------------------------
    COMPONENT	Adder_32bit
    PORT    (       -- begin port instantiation

		--==========================================================
                --
                --                      -- INPUTS --                    
                --
                --  AS_SEL:     Function select for addition and subtraction
                --              operations.
                --
                --  OpA:        First input operand. "WIDTH" bits wide.
                --
                --  OpB:        Second input operand. "WIDTH" bits wide.
                --
                --                      -- OUTPUTS -- 
                --
                --  CarryOut:   Carry out of most significant adder.
                --
                --  Carry_NMSA: Carry of the next most significant adder.
                --
                --  SUM :       SUM is the output of the adder. "WIDTH" bits
                --              wide.
                --
                --==========================================================
                
                AS_SEL	    :IN	    STD_LOGIC;
                OpA 	    :IN	    STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
		OpB 	    :IN	    STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
		CarryOut    :OUT    STD_LOGIC;
		Carry_NMSA  :OUT    STD_LOGIC;
		SUM	    :OUT    STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0)

            );      -- end port instantiation
    END COMPONENT;


    --=====================================================================
    ---- Test Signals -----------------------------------------------------

	---- Input Signals ----
    SIGNAL AS_tb    :STD_LOGIC;	-- Control signal for subtraction method
    SIGNAL OpA_tb   :STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
    SIGNAL OpB_tb   :STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);

	---- Output Signals ----

    SIGNAL Carry32_tb	:STD_LOGIC;  --Most significant carry
    SIGNAL Carry31_tb	:STD_LOGIC;  --Carry of MSB adder
    SIGNAL Sum_tb       :STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0); 


BEGIN

    -- Instance of circuit under test --
uut: Adder_32bit			
        PORT MAP (
                    AS_SEL 	=> AS_tb,
                    OpA   	=> OpA_tb,
		    OpB	        => OpB_tb,
		    CarryOut	=> Carry32_tb,
		    Carry_NMSA	=> Carry31_tb,
		    SUM		=> Sum_tb
		);

tb: PROCESS

    BEGIN

	test_runner_setup(runner, runner_cfg);  -- Start Vunit

	--test for any obvious faults--	
	AS_tb 	<= '0';
	OpA_tb 	<= x"00000000";
	OpB_tb	<= x"00000000";
	WAIT FOR PERIOD;
	ASSERT(	
		    (Carry32_tb = '0')
	        AND (Carry31_tb = '0') 
	        AND (Sum_tb = x"00000000")
	        )
	REPORT "Addition failed for "
		& "0x00000000 and 0x00000000" SEVERITY ERROR;

	--test to ensure carries worrk--	
	AS_tb 	<= '0';
	OpA_tb 	<= x"FFFFFFFF";
	OpB_tb	<= x"00000001";
	WAIT FOR PERIOD;
	ASSERT(
		    (Carry32_tb = '1') 
		AND (Carry31_tb = '1') 
		AND (Sum_tb = x"00000000")
	        )
	REPORT "Addition failed for "
		& "0xFFFFFFFF and 0x00000001" SEVERITY ERROR;
		
    	--Check reversability of operands-- 
	AS_tb 	<= '0';
	OpA_tb 	<= x"00000001";
	OpB_tb	<= x"FFFFFFFF";
	WAIT FOR PERIOD;
	ASSERT(
		    (Carry32_tb = '1') 
		AND (Carry31_tb = '1') 
		AND (Sum_tb = x"00000000")
		)
	REPORT "Addition failed for "
		& "0x00000001 and 0xFFFFFFFF" SEVERITY ERROR;
		
		
	--Ensure no carries are generated--
	AS_tb 	<= '0';
	OpA_tb 	<= x"AAAAAAAA";
	OpB_tb	<= x"55555555";
	WAIT FOR PERIOD;
	ASSERT(
		    (Carry32_tb = '0')
		AND (Carry31_tb = '0')
		AND (Sum_tb = x"FFFFFFFF")
		)
	REPORT "Addition failed for " 
		& "0xAAAAAAAA and 0x55555555" SEVERITY ERROR;

	--Check for most significant carry-- 
	AS_tb	<= '0';
	OpA_tb	<= x"CCCCCCCC";
	OpB_tb 	<= x"88888888";
	WAIT FOR PERIOD;
	ASSERT(
		(Carry32_tb = '1') 
		AND (Carry31_tb = '0') 
		AND (Sum_tb = x"55555554")
		)
	REPORT "Addition failed for "
		& "0xCCCCCCCC and 0x88888888" SEVERITY ERROR;

	--Check for second most significant carry--
	AS_tb 	<= '0';
	OpA_tb 	<= x"55555555";
	OpB_tb 	<= x"55555555";
	WAIT FOR PERIOD;
	ASSERT(	
		    (Carry32_tb = '1') 
		AND (Carry31_tb = '0') 
		AND (Sum_tb = x"AAAAAAAA")
		)
	REPORT "Addition failed for "
		& "0x55555555 and 0x55555555" SEVERITY ERROR;

	--Check random sum combination--
	AS_tb 	<= '0';
	OpA_tb 	<= x"00000001";
	OpB_tb	<= x"00000001";
	WAIT FOR PERIOD;
	ASSERT(
		    (Carry32_tb = '0') 
		AND (Carry31_tb = '0') 
		AND (Sum_tb = x"00000002")
	  	)
	REPORT "Addition failed for " 
		& "0x00000001 and 0x00000001" SEVERITY ERROR;

	--Check for any obvious faults within subtraction--
	AS_tb 	<= '1';
	OpA_tb 	<= x"00000000";
	OpB_tb	<= x"00000000";
	WAIT FOR PERIOD;
	ASSERT(
		    (Carry32_tb = '0') 
		AND (Carry31_tb = '0') 
		AND (Sum_tb = x"00000000")
		)
	REPORT "Subtraction failed for "
		& "0x00000000 and 0x00000000" SEVERITY ERROR;

	--Check ability to subtract--
	AS_tb 	<= '1';
	OpA_tb 	<= x"00000001";
	OpB_tb	<= x"00000001";
	WAIT FOR PERIOD;
	ASSERT(	
		    (Carry32_tb = '0') 
		AND (Carry31_tb = '0') 
		AND ( Sum_tb = x"00000000")
		)
	REPORT "Subtraction failed for "
		& "0x00000001 and 0x00000001" SEVERITY ERROR;

	--Check signed subtraction--
	AS_tb 	<= '1';
	OpA_tb 	<= x"00000000";
	OpB_tb	<= x"00000001";
	WAIT FOR PERIOD;
	ASSERT(
		    (Carry32_tb = '0') 
		AND (Carry31_tb = '0') 
		AND ( Sum_tb = x"FFFFFFFF")
	 	)
	REPORT "Subtraction failed for " 
		& " 0x00000000 and 0x00000001" SEVERITY ERROR;

	
	--Check subtraction method and most significant carry--
	AS_tb 	<= '1';
	OpA_tb	<= x"AAAAAAAA";
	OpB_tb	<= x"55555555";
	WAIT FOR PERIOD;
	ASSERT(
		    (Carry32_tb = '1') 
		AND (Carry31_tb	= '0') 
		AND (Sum_tb = x"55555555")
		)
	REPORT "Subtraction failed for " 
		& "0xAAAAAAAA and 0x55555555" SEVERITY ERROR;


	--Check subtraction method and second most significant carry--
	AS_tb 	<= '1';
	OpA_tb	<= x"55555555";
	OpB_tb 	<= x"AAAAAAAA";
	WAIT FOR PERIOD;
	ASSERT(
		    (Carry32_tb = '0') 
		AND (Carry31_tb = '1') 
		AND (Sum_tb = x"AAAAAAAAB")
		)
	REPORT "Subtraction failed for " 
		& "0xAAAAAAAA and 0x55555555" SEVERITY ERROR;

	--Check subtraction method for both upper carries--
	AS_tb 	<= '1';
	OpA_tb 	<= x"C0000000";
	OpB_tb 	<= x"90000000";
	WAIT FOR PERIOD;
	ASSERT(
	            (Carry32_tb = '1') 
		AND (Carry31_tb = '1') 
		AND (Sum_tb = x"300000000")
		)
	REPORT "Subtraction failed for " 
		& "0xC0000000 and 0x90000000" SEVERITY ERROR;	



	WAIT; -- suspend indefinitely

	test_runner_cleanup(runner);    -- Stop testbench

	END PROCESS;
END;
