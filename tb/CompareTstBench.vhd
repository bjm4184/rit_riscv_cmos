--  CompareTstBench.vhd
--  10/6/17
--  Andrew Eberhard
--
--  Testbench testing comparator and flags.
----------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE WORK.pkgConstants.ALL;

LIBRARY vunit_lib;
CONTEXT vunit_lib.vunit_context;

ENTITY CompareTstBench IS
    generic (runner_cfg : string);
END CompareTstBench;

ARCHITECTURE CompareTst OF CompareTstBench IS

    --======================================================================
    -- Component Declaration -----------------------------------------------
    COMPONENT CompareUnit_VHDL
    PORT   (   -- Begin component declaration
                
            --==============================================================
            --
            --                      -- INPUTS --
            --
            --  CarryOut:       CarryOut of the most significant adder.
            --
            --  CarryNMSA:      Next most significant carry. It is to be 
            --                  used for overflow calculation.
            --
            --  SUM:            Output of the "WIDTH" bit adder. Used for 
            --                  negative and zero calculations.
            --
            --  Opcode:         Opcode controlling the Branch control
            --                  multiplexer. "OP_WIDTH" bits wide.
            --
            --                      -- OUTPUTS --
            --
            --  CmpOut:         Output of the branch control multiplexer.
            --                  1-bit wide.
            --  
            --  SLT:            Set Less Than output. Routed out of unit and
            --                  and into the ALU output for register 
            --                  operations.
            --
            --  SLTU:           Set Less Than Unsigned output. Routed out of
            --                  unit and into the ALU output for register
            --                  operations.
            --
            --  N_FLAG:         Negative flag. Set when sum of adder is
            --                  negative.
            --
            --  V_FLAG:         Overflow flag. Set when the sum of adder 
            --                  cannot be represented by the size of the
            --                  register. Calculated by XOR of the carry
            --                  out of the adder the next most significant
            --                  carry.
            --
            --  C_FLAG:         Carry flag. Set when carry exists from 
            --                  operation within adder.
            --
            --  Z_FLAG:         Zero flag. Set when answer of adder is zero.
            --
            --==============================================================

            CarryOut    :IN     STD_LOGIC;
            CarryNMSA   :IN     STD_LOGIC;
            SUM         :IN     STD_LOGIC_VECTOR ((WIDTH - 1) DOWNTO 0);
            Opcode      :IN     STD_LOGIC_VECTOR ((OP_WIDTH - 1 ) DOWNTO 0);

            CmpOut      :OUT    STD_LOGIC;
            SLT         :OUT    STD_LOGIC;
            SLTU        :OUT    STD_LOGIC;
            N_FLAG      :OUT    STD_LOGIC;
            V_FLAG      :OUT    STD_LOGIC;
            C_FLAG      :OUT    STD_LOGIC;
            Z_FLAG      :OUT    STD_LOGIC
                
        );  -- End component declaration

    END COMPONENT;

    --======================================================================
    -- Control Signals -----------------------------------------------------
    SIGNAL  testCNT     :NATURAL        := 0; -- Test iteration

    --======================================================================
    -- Test Signals --------------------------------------------------------

        -- Input Signals --
    SIGNAL  CarryOut_tb     :STD_LOGIC;
    SIGNAL  CarryNMSA_tb    :STD_LOGIC;
    SIGNAL  SUM_tb          :STD_LOGIC_VECTOR ((WIDTH - 1) DOWNTO 0);
    SIGNAL  Opcode_tb       :STD_LOGIC_VECTOR ((OP_WIDTH - 1) DOWNTO 0);

        -- Ouput Signals --
    SIGNAL  CmpOut_tb       :STD_LOGIC;
    SIGNAL  SLT_tb          :STD_LOGIC;
    SIGNAL  SLTU_tb         :STD_LOGIC;
    SIGNAL  N_FLAG_tb       :STD_LOGIC;
    SIGNAL  V_FLAG_tb       :STD_LOGIC;
    SIGNAL  C_FLAG_tb       :STD_LOGIC;
    SIGNAL  Z_FLAG_tb       :STD_LOGIC;

        -- Test Signals --
    SIGNAL  Zero_Sig        :UNSIGNED(1 DOWNTO 0);
    SIGNAL  Temp_Sig        :UNSIGNED((WIDTH - 1) DOWNTO 0); 

BEGIN

-- Instantiate unit under test
uut:    CompareUnit_VHDL
    PORT MAP ( -- Begin Instantiation
            CarryOut    => CarryOut_tb,
            CarryNMSA   => CarryNMSA_tb,
            SUM         => SUM_tb,
            Opcode      => Opcode_tb,
            CmpOut      => CmpOut_tb,
            SLT         => SLT_tb,
            SLTU        => SLTU_tb,
            N_FLAG      => N_FLAG_tb,
            V_FLAG      => V_FLAG_tb,
            C_FLAG      => C_FLAG_tb,
            Z_FLAG      => Z_FLAG_tb
            ); -- End Instantiation
                    

tb: PROCESS

    VARIABLE    bitCNT      :INTEGER        := 0; -- bit counter

    BEGIN

	test_runner_setup(runner, runner_cfg);  -- Start Vunit

        --==================================================================
        -- TEST 1: Zero Flag = '0'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= "000";
        
        WAIT FOR PERIOD;
                
        -- Test Zero Flag
        Temp_Sig    <= unsigned(SUM_tb); 
        Zero_Sig    <= unsigned'('0' & Z_FLAG_tb);

        ASSERT (to_integer(Zero_Sig) = to_integer(Temp_Sig))
        REPORT "Zero flag incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test flags (N, C)
        ASSERT  (   (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb))
        REPORT  "Status (N, C) flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR; -- Test flags
        
        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb = (NOT CarryOut_tb)))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: no signal is muxed to input 000
        ASSERT  (CmpOut_tb = '0');
        REPORT "Error in compare unit output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;
        
        --==================================================================
        -- TEST 2: Zero Flag = '1'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"F0F0F0F0";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= "000";
        
        WAIT FOR PERIOD;
         
        -- Test Zero flag 
        Temp_Sig    <=  unsigned(SUM_tb);
        Zero_Sig    <=  unsigned'('0' & Z_FLAG_tb);

        ASSERT (to_integer(Zero_Sig) = to_integer(Temp_Sig))
        REPORT "Zero flag incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test flags (N, C)
        ASSERT  (   (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb))
        REPORT  "Status (N, C) flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR; 
        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb = (NOT CarryOut_tb)))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: no signal is muxed to input 000
        ASSERT  (CmpOut_tb = '0');
        REPORT "Error in compare unit output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 3: Overflow = '0' w CarryOut and CarryNMSA = '0' --
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"000";
        
        WAIT FOR PERIOD;
        
        -- Test Overflow Flag
        ASSERT (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb))
        REPORT "Overflow flag incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test flags (N, C, Z)
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb))
        REPORT  "Status (N, C, Z) flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;-- Test flags
        
        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb = (NOT CarryOut_tb)))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: no signal is muxed to input 000
        ASSERT  (CmpOut_tb = '0');
        REPORT "Error in compare unit output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 4: Overflow = '1' w CarryOut = '1'  and CarryNMSA = '0' --
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '1';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"000";
        
        WAIT FOR PERIOD;
        
        -- Test Overflow Flag
        ASSERT (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb))
        REPORT "Overflow flag incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test flags (N, C, Z)
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb))
        REPORT  "Status (N, C, Z) flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb = (NOT CarryOut_tb)))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: no signal is muxed to input 000
        ASSERT  (CmpOut_tb = '0');
        REPORT "Error in compare unit output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 5: Overflow = '1' w/ CarryOut = '0'  and CarryNMSA = '1' --
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '1';
        Opcode_tb       <= b"001";
        
        WAIT FOR PERIOD;
        
        -- Test Overflow Flag
        ASSERT (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb))
        REPORT "Overflow flag incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test flags (N, C, Z)
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb))
        REPORT  "Status (N, C, Z) flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb = (NOT CarryOut_tb)))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: no signal is muxed to input 000
        ASSERT  (CmpOut_tb = '0');
        REPORT "Error in compare unit output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;


        --==================================================================
        -- TEST 6: Overflow = '0' w/ CarryOut = '1' and CarryNMSA = '1' --
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '1';
        CarryNMSA_tb    <= '1';
        Opcode_tb       <= b"001";
        
        WAIT FOR PERIOD;
        
        -- Test flags: Zero, Carry are set
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb = (NOT (CarryOut_tb))))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: no signal is muxed to input 001
        ASSERT  (CmpOut_tb = '0');
        REPORT "Error in compare unit output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 7: BGEU = '0' --
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"010"; -- BGEU output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Zero
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb = (NOT (CarryOut_tb))))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output:BGEU
        ASSERT  (CmpOut_tb = CarryOut_tb)
        REPORT "Error in BGEU output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;
 
        --==================================================================
        -- TEST 8: BGEU = '1' --
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '1';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"010"; -- BGEU output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Overflow, Zero, Carry
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BGEU
        ASSERT  (CmpOut_tb = CarryOut_tb)
        REPORT "Error in BGEU output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 9: BLTU = '0' --
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '1';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"011"; -- BLTU output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Overflow, Zero, Carry
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BLTU
        ASSERT  (CmpOut_tb /= CarryOut_tb)
        REPORT "Error in BLTU output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 10: BLTU = '1' --
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"011"; -- BLTU output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Zero
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BLTU
        ASSERT  (CmpOut_tb /= CarryOut_tb)
        REPORT "Error in BLTU output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 11: BLT = '0' w/ Overflow = '0' & SUM(WIDTH - 1) = '0'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"100"; -- BLT output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Zero
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BLT 
        ASSERT  (CmpOut_tb = (SUM_tb(WIDTH - 1) XOR V_FLAG_TB))
        REPORT "Error in BLT output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;
 
        --==================================================================
        -- TEST 12: BLT = '1' w/ Overflow = '1' & SUM(WIDTH - 1) = '0'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '1';
        Opcode_tb       <= b"100"; -- BLT output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Overflow, Zero
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BLT 
        ASSERT  (CmpOut_tb = (SUM_tb(WIDTH - 1) XOR V_FLAG_TB))
        REPORT "Error in BLT output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 13: BLT = '1' w/ Overflow = '0' & SUM(WIDTH - 1) = '1'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"F0000000";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"100"; -- BLT output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Zero
        ASSERT  (   (Z_FLAG_tb = '0')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BLT 
        ASSERT  (CmpOut_tb = (SUM_tb(WIDTH - 1) XOR V_FLAG_TB))
        REPORT "Error in BLT output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 14: BLT = '1' w/ Overflow = '1' & SUM(WIDTH - 1) = '1'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"F0000000";
        CarryOut_tb     <= '1';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"100"; -- BLT output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Overflow, Zero, Carry
        ASSERT  (   (Z_FLAG_tb = '0')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BLT 
        ASSERT  (CmpOut_tb = (SUM_tb(WIDTH - 1) XOR V_FLAG_TB))
        REPORT "Error in BLT output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 15: BGE = '1' w/ Overflow = '0' and SUM(WIDTH - 1) = '0'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"101"; -- BGE output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Zero
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BGE
        ASSERT  (CmpOut_tb = (SUM_tb(WIDTH - 1) XNOR V_FLAG_TB))
        REPORT "Error in BGE output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 16: BGE = '0' w/ Overflow = '0' & SUM(WIDTH - 1) = '1'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"F0000000";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"101"; -- BGE output
        
        WAIT FOR PERIOD;
        
        -- Test flags:
        ASSERT  (   (Z_FLAG_tb = '0')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BGE 
        ASSERT  (CmpOut_tb = (SUM_tb(WIDTH - 1) XNOR V_FLAG_TB))
        REPORT "Error in BLT output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 17: BGE = '0' w/ Overflow = '1' & SUM(WIDTH - 1) = '0'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '1';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"101"; -- BGE output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Zero, Carry, Overflow
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BGE 
        ASSERT  (CmpOut_tb = (SUM_tb(WIDTH - 1) XNOR V_FLAG_TB))
        REPORT "Error in BLT output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 18: BGE = '1' w/ Overflow = '1' & SUM(WIDTH - 1) = '1'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"F0000000";
        CarryOut_tb     <= '1';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"101"; -- BGE output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Zero, Carry, Overflow
        ASSERT  (   (Z_FLAG_tb = '0')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BGE 
        ASSERT  (CmpOut_tb = (SUM_tb(WIDTH - 1) XNOR V_FLAG_TB))
        REPORT "Error in BLT output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 19: BNE = '0' w/ Zero Flag = '1'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000000";
        CarryOut_tb     <= '1';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"110"; -- BNE output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Zero, Carry, Overflow
        ASSERT  (   (Z_FLAG_tb = '1')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BNE 
        ASSERT  (CmpOut_tb /= Z_FLAG_tb)
        REPORT "Error in BLT output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 20: BNE = '1' w/ Zero Flag = '0'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000001";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"110"; -- BNE output
        
        WAIT FOR PERIOD;
        
        -- Test flags:
        ASSERT  (   (Z_FLAG_tb = '0')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BNE 
        ASSERT  (CmpOut_tb /= Z_FLAG_tb)
        REPORT "Error in BLT output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 21: BEQ = '0' w/ Zero Flag = '0'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000001";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"111"; -- BNE output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Zero
        ASSERT  (   (Z_FLAG_tb = '0')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb((WIDTH - 1)) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BEQ 
        ASSERT  (CmpOut_tb = Z_FLAG_tb)
        REPORT "Error in BLT output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        --==================================================================
        -- TEST 22: BEQ = '1' w/ Zero Flag = '1'--
        testCNT <= testCNT + 1;

        SUM_tb          <= x"00000001";
        CarryOut_tb     <= '0';
        CarryNMSA_tb    <= '0';
        Opcode_tb       <= b"111"; -- BEQ output
        
        WAIT FOR PERIOD;
        
        -- Test flags: Zero
        ASSERT  (   (Z_FLAG_tb = '0')
                AND (N_FLAG_tb = SUM_tb(WIDTH - 1))
                AND (C_FLAG_tb = CarryOut_tb)
                AND (V_FLAG_tb = (CarryOut_tb XOR CarryNMSA_tb)))
        REPORT  "Status flags were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test SLT and SLTU
        ASSERT  (   (SLT_tb = (SUM_tb(WIDTH - 1) XOR (V_FLAG_tb)))
                AND (sltu_tb /= CarryOut_tb))
        REPORT "SLT or SLTU were incorrect on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;

        -- Test comparator mux output: BEQ
        ASSERT  (CmpOut_tb = Z_FLAG_tb)
        REPORT "Error in BLT output on test "
                & NATURAL'image(testCNT) & "." SEVERITY ERROR;
        
        --==================================================================

        WAIT; --Suspend Indefinitely

	test_runner_cleanup(runner);    -- Stop testbench

    END PROCESS;
END;
