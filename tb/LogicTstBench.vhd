--  LogicTstBench.vhd
--  10/5/17
--  Andrew Eberhard
--
--  Testbench proving the operations (AND, OR, XOR) held by the logic unit.
----------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.pkgConstants.ALL;

LIBRARY vunit_lib;
CONTEXT vunit_lib.vunit_context;

ENTITY LogicTestBench IS
    generic (runner_cfg : string);
END LogicTestBench;

ARCHITECTURE LogicTest OF LogicTestBench IS

    --======================================================================
    --- Component Declaration ----------------------------------------------

    COMPONENT	LogicUnit_vhdl
    PORT    (	-- Begin port declaration

            --==============================================================
            --  
            --                      -- INPUTS --
            --
            --  OpA:        First input operand to the logic unit. "WIDTH" 
            --              bits wide.
            --
            --  OpB:        Second input operand to the logic unit. "WIDTH"
            --              bits wide.
            --
            --  AND_out:    Output of the bitwise AND operation. "WIDTH"
            --              bits wide.
            -- 
            --                      -- OUTPUTS --
            --
            --  OR_out:     Output of the bitwise OR operation. "WIDTH"
            --              bits wide.
            --  
            --  XOR_out:    Output of the bitwise XOR operation. "WIDTH"
            --              bits wide.
            --
            --==============================================================

            OpA         :IN     STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
	    OpB		:IN	STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
	    AND_out	:OUT	STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
	    OR_out	:OUT	STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
	    XOR_out	:OUT	STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0)

	    ); 	-- End port Declaration
    END COMPONENT;

    --======================================================================
    ---- Test Signals ------------------------------------------------------

    ---- Input Signals ----
    SIGNAL 	OpA_tb		:STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
    SIGNAL 	OpB_tb		:STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);


    ---- Output Signals ----
    SIGNAL	AND_tb		:STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
    SIGNAL	OR_tb		:STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
    SIGNAL	XOR_tb		:STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
		


BEGIN

--Instantiate unit under test
uut: 	LogicUnit_vhdl	
            PORT MAP ( -- BEGIN PORT MAP INSTANTIATION
			OpA 	=> OpA_tb,
			OpB 	=> OpB_tb,
			AND_out => AND_tb,
			OR_out	=> OR_tb,
			XOR_out => XOR_tb
		    ); -- END PORT MAP INSTANTIATION

-- Define test bench process
tb: PROCESS

    BEGIN

	test_runner_setup(runner, runner_cfg);  -- Start Vunit

	-- test all default operations --
        -- TEST:
            -- Ensure no bit is set for any of the three operations.
	OpA_tb	<= x"00000000";
	OpB_tb	<= x"00000000";
	WAIT FOR PERIOD;
	ASSERT ((AND_tb = x"00000000"))
	REPORT "AND failed for 0x00000000 and 0x0000000" SEVERITY ERROR;
	ASSERT ((Or_tb	= x"00000000"))
	REPORT "OR  failed for 0x00000000 and 0x0000000" SEVERITY ERROR;
	ASSERT ((XOR_tb = x"00000000"))
	REPORT "XOR failed for 0x00000000 and 0x0000000" SEVERITY ERROR;
	
	-- test OR and XOR  operations --        
        -- TEST:
            -- Output should be cleared by second operand on bitwise AND
            -- Output should be set by first operand on bitwise OR
            -- Output shoudl be set by operands on bitwise XOR

	OpA_tb	<= x"FFFFFFFF";
	OpB_tb	<= x"00000000";
	WAIT FOR PERIOD;
	ASSERT ((AND_tb = x"00000000"))
	REPORT "AND failed for 0xFFFFFFFF and 0x0000000" SEVERITY ERROR;
	ASSERT ((Or_tb	= x"FFFFFFFF"))
	REPORT "OR  failed for 0xFFFFFFFF and 0x0000000" SEVERITY ERROR;
	ASSERT ((XOR_tb = x"FFFFFFFF"))
	REPORT "XOR failed for 0xFFFFFFFF and 0x0000000" SEVERITY ERROR;

	-- test reverse operations --
        -- TEST:
            -- Output should be cleared by first operand on bitwise AND
            -- Output should be set by second operand on bitwise OR
            -- Output shoudl be set by operands on bitwise XOR
	OpA_tb	<= x"00000000";
	OpB_tb	<= x"FFFFFFFF";
	WAIT FOR PERIOD;
	ASSERT ((AND_tb = x"00000000"))
	REPORT "AND failed for 0x00000000 and 0xFFFFFFF" SEVERITY ERROR;
	ASSERT ((Or_tb	= x"FFFFFFFF"))
	REPORT "OR  failed for 0x00000000 and 0xFFFFFFF" SEVERITY ERROR;
	ASSERT ((XOR_tb = x"FFFFFFFF"))
	REPORT "XOR failed for 0x00000000 and 0xFFFFFFF" SEVERITY ERROR;

	-- test all default operations --
        -- TEST:
            -- Output should be clear for bitwise AND
            -- All bits should be set for bitwise OR
            -- All bits should be set for bitwise XOR
	OpA_tb	<= x"AAAAAAAA";
	OpB_tb	<= x"55555555";
	WAIT FOR PERIOD;
	ASSERT ((AND_tb = x"00000000"))
	REPORT "AND failed for 0xAAAAAAAA and 0x5555555" SEVERITY ERROR;
	ASSERT ((Or_tb	= x"FFFFFFFF"))
	REPORT "OR  failed for 0xAAAAAAAA and 0x5555555" SEVERITY ERROR;
	ASSERT ((XOR_tb = x"FFFFFFFF"))
	REPORT "XOR failed for 0xAAAAAAAA and 0x5555555" SEVERITY ERROR;

	-- test all default operations --
        -- TEST:
            -- Output should be reflected inputs for bitwise AND
            -- Output should be reflected inputs for bitwise OR
            -- All bits should be cleared for bitwise XOR
	OpA_tb	<= x"AAAAAAAA";
	OpB_tb	<= x"AAAAAAAA";
	WAIT FOR PERIOD;
	ASSERT ((AND_tb = x"AAAAAAAA"))
	REPORT "AND failed for 0xAAAAAAAA and 0xAAAAAAA" SEVERITY ERROR;
	ASSERT ((Or_tb	= x"AAAAAAAA"))
	REPORT "OR  failed for 0xAAAAAAAA and 0xAAAAAAA" SEVERITY ERROR;
	ASSERT ((XOR_tb = x"00000000"))
	REPORT "XOR failed for 0xAAAAAAAA and 0xAAAAAAA" SEVERITY ERROR;

	-- test all default operations --
        -- TEST:
            -- MSB and every other bit set for bitwise AND
            -- All bits set for bitwise OR
            -- LSB and every other bit set for bitwise OR
	OpA_tb	<= x"FFFFFFFF";
	OpB_tb	<= x"AAAAAAAA";
	WAIT FOR PERIOD;
	ASSERT ((AND_tb = x"AAAAAAAA"))
	REPORT "AND failed for 0xFFFFFFFF and 0xAAAAAAA" SEVERITY ERROR;
	ASSERT ((Or_tb	= x"AAAAAAAA"))
	REPORT "OR  failed for 0xFFFFFFFF and 0xAAAAAAA" SEVERITY ERROR;
	ASSERT ((XOR_tb = x"55555555"))
	REPORT "XOR failed for 0xFFFFFFFF and 0xAAAAAAA" SEVERITY ERROR;

	
	WAIT;

	test_runner_cleanup(runner);    -- Stop testbench

    END PROCESS;
END;

