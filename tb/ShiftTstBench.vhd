--  ShiftTstBench.vhd
--  10/5/17
--  Andrew Eberhard
--
-- A testbench for a 32-bit shifter for left and right shift operations.
----------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE WORK.pkgConstants.ALL;

LIBRARY vunit_lib;
CONTEXT vunit_lib.vunit_context;

ENTITY shiftTstBench IS
    generic (runner_cfg : string);
END shiftTstBench;

ARCHITECTURE shiftTest OF shiftTstBench IS

    --======================================================================
    --- Component Declaration ----------------------------------------------

    COMPONENT	ShiftUnit_vhdl
    PORT (  	-- Begin Port Definition

            --==============================================================
            --
            --                  -- INPUTS --
            --
            --  RL_A_CNTRL:     Function select for arithmetic and logical
            --                  right shift.
            --
            --  OpA:            First input operand. "WIDTH" bits wide.
            --
            --                  -- OUTPUTS --
            --  
            --  LEFT_SHIFT:     Output of the left shift shifting unit.
            --                  "WIDTH" bits wide.
            --
            --  RIGHT_SHIFT:    Output of the right shift shifting unit.
            --                  "WIDTH" bits wide.
            --
            --==============================================================

            RL_A_CNTRL	:IN 	STD_LOGIC;
	    OpA		:IN	STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
	    LEFT_SHIFT	:OUT	STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
	    RIGHT_SHIFT	:OUT	STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0)
	
        );       -- End Port Definition
    END COMPONENT;

    --======================================================================
    ---- Test Signals ------------------------------------------------------

    ---- Input Signals ----
    SIGNAL 	RL_A_CNTRL_tb	:STD_LOGIC;
    SIGNAL	OpA_tb		:STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
    
    ---- Output Signals ----
    SIGNAL	Left_Shift_tb	:STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
    SIGNAL	Right_Shift_tb	:STD_LOGIC_VECTOR((WIDTH - 1) DOWNTO 0);
	
    ---- Test Signals ----
    SIGNAL      Shift_Sig       :UNSIGNED((WIDTH - 1) DOWNTO 0);

BEGIN

--Declare unit under test
uut: 	ShiftUnit_vhdl	
            PORT MAP ( -- BEGIN PORT MAP DECLARATION
                        RL_A_CNTRL	=> RL_A_CNTRL_tb,	
			OpA 		=> OpA_tb,
			Left_Shift	=> Left_Shift_tb,
			Right_Shift	=> Right_Shift_tb
		    ); -- END PORT MAP DECLARATION

tb: PROCESS
	
    BEGIN

	test_runner_setup(runner, runner_cfg);  -- Start Vunit

	-- TEST LOGICAL LEFT SHIFT --
	RL_A_CNTRL_tb	<= '0';
	OpA_tb		<= x"00000000";

	FOR i IN 0 TO (WIDTH - 1) LOOP

            Shift_Sig  <=   SHIFT_LEFT(unsigned(OpA_tb), 1);

	    WAIT FOR PERIOD;
	    ASSERT (unsigned(OpA_tb) = Shift_Sig)
	    REPORT "Left shift failed for: " & 
		    integer'image(to_integer(unsigned(OpA_tb))) &
		    "on" & natural'image(i) & "attempt." SEVERITY ERROR;
	
	    IF (i = 0) THEN
		OpA_tb      <=  x"00000001";
                Shift_Sig   <=  x"00000001";
	    END IF;

	END LOOP;

	-- TEST LOGICAL RIGHT SHIFT --
	RL_A_CNTRL_tb 	<= '0';
	OpA_tb		<= x"00000000";
	
	FOR i IN 0 TO (WIDTH - 1) LOOP

            Shift_Sig   <=  SHIFT_RIGHT(unsigned(OpA_tb), 1);
	    
            WAIT FOR PERIOD;
	    ASSERT (unsigned(OpA_tb) = Shift_Sig)
	    REPORT "Right shift failed for: " &
		    integer'image(to_integer(unsigned(OpA_tb))) &
		    " on " & natural'image(i) & "iteration." SEVERITY ERROR;

	    IF (i = 0) THEN
		OpA_tb      <=  x"80000000";
                Shift_Sig   <=  x"80000000";
	    END IF;

	END LOOP;

	-- TEST ARITHMETIC RIGHT SHIFT w/ '1' as shift in --
	RL_A_CNTRL_tb 	<= '1';
	OpA_tb		<= x"80000000"; -- MSB = '1' --
	
	FOR i IN 0 TO (WIDTH - 1) LOOP

            Shift_Sig   <=  SHIFT_RIGHT(unsigned(OpA_tb OR x"8000000"), 1);

	    WAIT FOR PERIOD;
	    ASSERT (unsigned(OpA_tb) = Shift_Sig)
	    REPORT  "RIGHT shift failed for: " & 
		    integer'image(to_integer(unsigned(OpA_tb))) &
		    " on " & natural'image(i) & "iteration." SEVERITY ERROR;

	END LOOP;

        -- TEST ARITHMETIC RIGHT SHIFT w/ '0' as shift in --
        RL_A_CNTRL_tb   <= '1';
        OpA_tb          <= x"7FFFFFFF"; -- MSB = '0' -- 

        FOR i IN 0 TO (WIDTH - 1) LOOP

            Shift_Sig   <= shift_right(unsigned(OpA_tb AND x"FFFFFFFF"), 1);
            
            WAIT FOR PERIOD;
            ASSERT (unsigned(OpA_tb) = Shift_Sig)
            REPORT  "RIGHT shift failed for: " &
                    integer'image(to_integer(unsigned(OpA_tb))) &
                    " on " & natural'image(i) & "iteration." SEVERITY ERROR;

        END LOOP;
	
	WAIT;

	test_runner_cleanup(runner);    -- Stop testbench

    END PROCESS;
END;
