--  PC_datapath_tb.vhd
--  9/23/17
--  Braeden Morrison
--
--  A testbench for the portion of the datapath around the program counter.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity PC_datapath_tb is
    generic (runner_cfg : string);
end PC_datapath_tb;

architecture test of PC_datapath_tb is

    --=========================================================================
    ---- Defined Constants ----------------------------------------------------

    constant CLK_PERIOD :       time := 50 ns;      -- Clock Period
    constant WIDTH :            natural := 32;      -- Width of the datapath
    constant BRANCH_IMM_BITS :  natural := 12;      -- Size of branch immediate
    constant JAL_IMM_BITS :     natural := 20;      -- Size of JAL immediate
    constant JALR_IMM_BITS :    natural := 12;      -- Size of JALR immediate
    constant RUN_ITERATIONS :   natural := 256;     -- Number of times to run
                                                    -- the PC in-between tests
    --=========================================================================
    ---- Component Declarations -----------------------------------------------

    component PC_datapath is
        generic (
            --=================================================================
            -- WIDTH:           The width of the datapath and registers
            -- BRANCH_IMM_BITS: The size of the immediate for a branch
            -- JAL_IMM_BITS:    The size of the immediate for a JAL
            -- JALR_IMM_BITS:   The size of the immediate for a JALR
            --=================================================================

            WIDTH :             natural := 32; 
            BRANCH_IMM_BITS :   natural := 12;
            JAL_IMM_BITS :      natural := 20;
            JALR_IMM_BITS :     natural := 12
        );
        port (
            --=================================================================
            ---- Input Signals ----

            -- branch_imm:  The signed immediate value used to calculate the 
            --                  branch destination. Comes from instruction 
            --                  decode.
            -- jal_imm:     The immediate value that will be sign extended and
            --                  used to calculate the destination for jump. 
            --                  Comes from instruction decode.
            -- jalr_imm:    The signed immediate value that gets added to the
            --                  'jalr_reg' value to determine jump destination.
            --                  Comes from instruction decode.
            -- jalr_reg:    The value from the register file that gets added to
            --                  'jalr_imm' to determine the destination for 
            --                  JALRs. Comes from the register-file.

            -- jmp_ctrl:    A two-bit signal used to select the next value for 
            --                  PC. Comes from the control unit.

            -- clk:         A one-bit clock signal that gets sent to the PC. 
            --                  Comes from the clock circuitry.
            -- pc_rst_n:    A one-bit active-low reset signal for the PC. I'm 
            --                  not really sure where this one comes from, but 
            --                  it could even be from the power-supply or 
            --                  something.

            ---- Output Signals ----

            -- cur_pc:      The current value in the PC register. Goes to the
            --                  register-file datapath.
            -- next_pc:     The next value for the PC assuming to jumps or 
            --                  branches (Is equal to cur_pc + 4). Goes to the 
            --                  resgiter-file datapath.
            --=================================================================

            branch_imm :     in  std_logic_vector(BRANCH_IMM_BITS-1 downto 0);
            jal_imm :        in  std_logic_vector(JAL_IMM_BITS-1 downto 0);
            jalr_imm :       in  std_logic_vector(JALR_IMM_BITS-1 downto 0);
            jalr_reg :       in  std_logic_vector(WIDTH-1 downto 0);

            jmp_ctrl :       in  std_logic_vector(1 downto 0);

            clk :            in  std_logic;
            pc_rst_n :       in  std_logic;
        
            cur_pc :         out std_logic_vector(WIDTH-1 downto 0); 
            next_pc :        out std_logic_vector(WIDTH-1 downto 0)
        );
    end component PC_datapath;

    --=========================================================================
    ---- Test Signals ---------------------------------------------------------

    ---- Input Signals ----

    signal branch_imm :     std_logic_vector(BRANCH_IMM_BITS-1 downto 0) 
                                                := (others => '0');
    signal jal_imm :        std_logic_vector(JAL_IMM_BITS-1 downto 0)
                                                := (others => '0');
    signal jalr_imm :       std_logic_vector(JALR_IMM_BITS-1 downto 0)
                                                := (others => '0');
    signal jalr_reg :       std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');

    signal jmp_ctrl :       std_logic_vector(1 downto 0)
                                                := (others => '0');

    signal clk :            std_logic := '0';
    signal pc_rst_n :       std_logic := '0';
    
    ---- Output Signals ----

    signal cur_pc :         std_logic_vector(WIDTH-1 downto 0); 
    signal next_pc :        std_logic_vector(WIDTH-1 downto 0);

begin

    --=========================================================================
    ---- Component Instances --------------------------------------------------

    UUT : PC_datapath
        port map (
            -- Inputs --
            branch_imm  => branch_imm,
            jal_imm     => jal_imm,
            jalr_imm    => jalr_imm,
            jalr_reg    => jalr_reg,

            jmp_ctrl    => jmp_ctrl,

            clk         => clk,
            pc_rst_n    => pc_rst_n,

            -- Outputs --
            cur_pc      => cur_pc,
            next_pc     => next_pc
        );

    --=========================================================================
    ---- Test Processes -------------------------------------------------------

    -- Process that controls the clock
    clk_proc : process
    begin
        clk <= NOT clk;
        wait for CLK_PERIOD/2;
    end process;

    -- Process that runs some test cases on the datapath
    stim_proc : process
        -- exp_addr :       The expected value for the PC
        variable exp_addr : unsigned(WIDTH-1 downto 0) := (others => '0');
    begin

        test_runner_setup(runner, runner_cfg);  -- Start Vunit

        --==== Reset Test ====---
        pc_rst_n <= '0';        -- enable the PC

        wait for CLK_PERIOD/2;

        assert (cur_pc = std_logic_vector(to_unsigned(0, WIDTH)))
            report "Failed to reset Program Counter!"
            severity error; 

        assert (unsigned(next_pc) = (unsigned(cur_pc) + 4))
            report "'next_pc' incorrect after reset!"
            severity error;

        --==== PC run ====--
        for i in 1 to RUN_ITERATIONS loop
            exp_addr := unsigned(cur_pc) + 4;
            pc_rst_n <= '1';
            jmp_ctrl <= "00"; -- No branches or jumps
            wait for CLK_PERIOD;

            assert (cur_pc = std_logic_vector(exp_addr))
                report "Incorrect value for 'cur_pc' while running!"
                severity error;

            assert (next_pc = std_logic_vector(exp_addr + 4))
                report "Incorrect value for 'next_pc' while running!"
                severity error;
        end loop;

        --==== Zero the PC ====--
        jalr_imm <= (others => '0');
        jalr_reg <= (others => '0');
        jmp_ctrl <= "10"; -- indicates a JALR
        wait for CLK_PERIOD;

        assert (cur_pc = std_logic_vector(to_unsigned(0, WIDTH)))
            report "Failed to zero Program Counter!"
            severity error; 

        assert (unsigned(next_pc) = (unsigned(cur_pc) + 4))
            report "'next_pc' incorrect after zero!"
            severity error;

        wait for CLK_PERIOD;

        --==== JAL forward ====--
        jal_imm  <= X"12345";
        exp_addr := X"0001_2345"; -- The sign extended constant
        exp_addr := exp_addr + unsigned(cur_pc); -- JAL adds the imm to the PC
        jmp_ctrl <= "11"; -- represents a JAL
        wait for CLK_PERIOD;

        assert (cur_pc = std_logic_vector(exp_addr))
            report "Incorrect 'cur_pc' after forward JAL!"
            severity error; 

        assert (unsigned(next_pc) = (unsigned(cur_pc) + 4))
            report "Incorrect 'next_pc' after forward JAL!"
            severity error;

        --==== JAL backward ====--
        jal_imm  <= X"FADED";
        exp_addr := X"FFFF_ADED"; -- The sign extended constant
        exp_addr := exp_addr + unsigned(cur_pc);
        jmp_ctrl <= "11"; -- represents a JAL
        wait for CLK_PERIOD;

        assert (cur_pc = std_logic_vector(exp_addr)) 
            report "Incorrect 'cur_pc' after backward JAL!"
            severity error; 

        assert (unsigned(next_pc) = (unsigned(cur_pc) + 4))
            report "Incorrect 'next_pc' after backward JAL!"
            severity error;

        --==== PC run ====--
        for i in 1 to RUN_ITERATIONS loop
            exp_addr := unsigned(cur_pc) + 4;
            pc_rst_n <= '1';
            jmp_ctrl <= "00"; -- No branches or jumps
            wait for CLK_PERIOD;

            assert (cur_pc = std_logic_vector(exp_addr))
                report "Incorrect value for 'cur_pc' while running!"
                severity error;

            assert (next_pc = std_logic_vector(exp_addr + 4))
                report "Incorrect value for 'next_pc' while running!"
                severity error;
        end loop;

        --==== Branch forward ====--
        branch_imm <= X"369";
        exp_addr := X"0000_0369";
        exp_addr := exp_addr + unsigned(cur_pc);
        jmp_ctrl <= "01"; -- Branch instruction
        wait for CLK_PERIOD;

        assert (cur_pc = std_logic_vector(exp_addr))
            report "Incorrect 'cur_pc' after forward Branch!"
            severity error; 

        assert (unsigned(next_pc) = (unsigned(cur_pc) + 4))
            report "Incorrect 'next_pc' after forward Branch!"
            severity error;

        --==== Branch backward ====--
        branch_imm <= X"DAD";
        exp_addr := X"FFFF_FDAD";
        exp_addr := exp_addr + unsigned(cur_pc);
        jmp_ctrl <= "01"; -- Branch instruction
        wait for CLK_PERIOD;

        assert (cur_pc = std_logic_vector(exp_addr)) 
            report "Incorrect 'cur_pc' after backward Branch!"
            severity error; 

        assert (unsigned(next_pc) = (unsigned(cur_pc) + 4))
            report "Incorrect 'next_pc' after backward Branch!"
            severity error;

        --==== PC run ====--
        for i in 1 to RUN_ITERATIONS loop
            exp_addr := unsigned(cur_pc) + 4;
            pc_rst_n <= '1';
            jmp_ctrl <= "00"; -- No branches or jumps
            wait for CLK_PERIOD;

            assert (cur_pc = std_logic_vector(exp_addr))
                report "Incorrect value for 'cur_pc' while running!"
                severity error;

            assert (next_pc = std_logic_vector(exp_addr + 4))
                report "Incorrect value for 'next_pc' while running!"
                severity error;
        end loop;

        --==== JALR forward ====--
        jalr_reg <= X"DEAD_BEEF";
        exp_addr := X"DEAD_BEEF";
        jalr_imm <= X"321";
        exp_addr := exp_addr + X"0000_0321";
        jmp_ctrl <= "10"; -- JALR instruction
        wait for CLK_PERIOD;

        assert (cur_pc = std_logic_vector(exp_addr))
            report "Incorrect 'cur_pc' after forward JALR!"
            severity error; 

        assert (unsigned(next_pc) = (unsigned(cur_pc) + 4))
            report "Incorrect 'next_pc' after forward JALR!"
            severity error;

        --==== JALR backward ====--
        jalr_reg <= X"CAFE_BABE";
        exp_addr := X"CAFE_BABE";
        jalr_imm <= X"BAD";
        exp_addr := exp_addr + X"FFFF_FBAD";
        jmp_ctrl <= "10"; -- JALR instruction
        wait for CLK_PERIOD;

        assert (cur_pc = std_logic_vector(exp_addr)) 
            report "Incorrect 'cur_pc' after backward JALR!"
            severity error; 

        assert (unsigned(next_pc) = (unsigned(cur_pc) + 4))
            report "Incorrect 'next_pc' after backward JALR!"
            severity error;

        --==== PC run ====--
        for i in 1 to RUN_ITERATIONS loop
            exp_addr := unsigned(cur_pc) + 4;
            pc_rst_n <= '1';
            jmp_ctrl <= "00"; -- No branches or jumps
            wait for CLK_PERIOD;

            assert (cur_pc = std_logic_vector(exp_addr))
                report "Incorrect value for 'cur_pc' while running!"
                severity error;

            assert (next_pc = std_logic_vector(exp_addr + 4))
                report "Incorrect value for 'next_pc' while running!"
                severity error;
        end loop;

        test_runner_cleanup(runner);    -- Stop testbench
    end process;

end test;
