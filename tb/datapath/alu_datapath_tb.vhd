--  alu_datapath_tb.vhd
--  10/5/17
--  Braeden Morrison
--
--  A testbench for the portion of the datapath around the alu.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity alu_datapath_tb is
    generic (runner_cfg : string);
end alu_datapath_tb;

architecture test of alu_datapath_tb is

    --=========================================================================
    ---- Defined Constants ----------------------------------------------------

    constant CLK_PERIOD :       time := 50 ns;      -- Clock Period
    constant WIDTH :            natural := 32;      -- Width of the datapath
    constant IMM_BITS :         natural := 12;      -- Size of the immediate.
    constant OP_BITS :          natural := 3;       -- Bits for the ALU opcode

    --=========================================================================
    ---- Component Declarations -----------------------------------------------

    component alu_datapath
        generic (
            --=================================================================
            -- WIDTH:           The word size for the processor (32 bits).
            -- OP_BITS:         The number of bits in the ALU opcode.
            -- IMM_BITS:        The size of the immediate operand.
            --=================================================================
            WIDTH :             natural := 32;
            IMM_BITS :          natural := 12;
            OP_BITS :           natural := 3
        );
        port (
            --=================================================================
            ---- Input Signals ----

            -- reg_in1:     The first input from the register file. Serves as
            --                  the first operand and connects to rs1_out from
            --                  the reg file.
            -- reg_in2:     The second input from the register file. Serves as
            --                  the second operand in register-register 
            --                  operations. Comes from rs2_out of the reg file.
            -- imm_in:      The immediate used in register-immediate 
            --                  operations. Comes from instruction decode.
            --
            -- alu_in2_sel: The control signal that determines whether the 
            --                  operand to the ALU is an immediate or register
            --                  value. Comes from the control unit.
            -- alu_ctrl:    The control signal that selects between both 
            --                  addition and subtraction and between arithmetic
            --                  and logical right shifting. Comes from the 
            --                  control unit.
            -- alu_op:      The opcode for the ALU. Determines which operation
            --                  is performed. Comes from the control unit.

            ---- Output Signals ----

            -- alu_out:     The primary output of the ALU. Goes to the reg file
            --                  and the memory datapaths.
            -- compare_out: The secondary output of the ALU. Indicates whether
            --                  or not a branch should be taken during a branch
            --                  instruction.
            --=================================================================

            reg_in1 :       in  std_logic_vector(WIDTH-1 downto 0);
            reg_in2 :       in  std_logic_vector(WIDTH-1 downto 0);
            imm_in :        in  std_logic_vector(IMM_BITS-1 downto 0);

            alu_in2_sel :   in  std_logic;
            alu_ctrl :      in  std_logic;
            alu_op :        in  std_logic_vector(OP_BITS-1 downto 0);

            alu_out :       out std_logic_vector(WIDTH-1 downto 0);
            compare_out :   out std_logic
        );
    end component alu_datapath;

    --=========================================================================
    ---- Test Signals ---------------------------------------------------------

    ---- Input Signals ----

    signal reg_in1 :        std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');
    signal reg_in2 :        std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');
    signal imm_in :         std_logic_vector(IMM_BITS-1 downto 0)
                                                := (others => '0');


    signal alu_in2_sel :    std_logic := '0';
    signal alu_ctrl :       std_logic := '0';
    signal alu_op :         std_logic_vector(OP_BITS-1 downto 0)
                                                := (others => '0');

    ---- Output Signals ----

    signal alu_out :        std_logic_vector(WIDTH-1 downto 0);
    signal compare_out :    std_logic;

begin

    --=========================================================================
    ---- Component Instances --------------------------------------------------

    UUT : alu_datapath
        port map (
            -- Inputs --
            reg_in1     => reg_in1,
            reg_in2     => reg_in2,
            imm_in      => imm_in,

            alu_in2_sel => alu_in2_sel,
            alu_ctrl    => alu_ctrl,
            alu_op      => alu_op,

            -- Outputs --
            alu_out     => alu_out,
            compare_out => compare_out
        );

    --=========================================================================
    ---- Test Processes -------------------------------------------------------

    -- Process that runs some test cases on the datapath
    stim_proc : process
    begin

        test_runner_setup(runner, runner_cfg);  -- Start Vunit

        ---- Test a register-register addition ----
        reg_in1 <= X"0123_4567";
        reg_in2 <= X"FEDC_BA98";
        imm_in  <= X"000";

        alu_in2_sel <= '0';
        alu_ctrl <= '0';
        alu_op <= "000";    -- opcode for addition
        wait for CLK_PERIOD;

        assert (alu_out = X"FFFF_FFFF")
            report "Incorrect result for register-register addition!"
            severity error;

        ---- Test a register-immediate addition ----
        -- Ensures that alu_in2_sel works properly.
        reg_in1 <= X"00BE_B000";
        reg_in2 <= X"0000_0000";
        imm_in  <= X"AD1";

        alu_in2_sel <= '1';
        alu_ctrl <= '0';
        alu_op <= "000";    -- opcode for addition
        wait for CLK_PERIOD;

        assert (alu_out = X"00BE_BAD1")
            report "Incorrect result for register-immediate addition!"
            severity error;

        ---- Test an arithmetic right-shift ----
        -- Ensures that alu_ctrl and alu_op are properly connected.
        reg_in1 <= X"EEDB_EEF0";
        reg_in2 <= X"0000_0004";
        imm_in  <= X"000";

        alu_in2_sel <= '0';
        alu_ctrl <= '1';    -- perform an arithmetic shift
        alu_op <= "101";    -- opcode for a right-shift
        wait for CLK_PERIOD;

        assert (alu_out = X"FEED_BEEF")
            report "Incorrect result for arithmetic right-shift!"
            severity error;

        ---- Test BGE and BLT ----
        -- Ensures that compare_out is properly connected.
        reg_in1 <= X"0000_0005";
        reg_in2 <= X"0000_0005";

        alu_in2_sel <= '0';
        alu_ctrl <= '1';    -- required for branch operations
        alu_op <= "101";    -- opcode for a BGE
        wait for CLK_PERIOD;

        assert (compare_out = '1')
            report "Incorrect compare result for BGE!"
            severity error;

        alu_op <= "100";    -- opcode for a BLT
        wait for CLK_PERIOD;

        assert (compare_out = '0')
            report "Incorrect compare result for BLT!"
            severity error;

        ---- Testing complete ----
        test_runner_cleanup(runner);    -- Stop testbench
    end process;

end test;
