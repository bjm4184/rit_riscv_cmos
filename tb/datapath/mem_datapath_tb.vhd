--  mem_datapath_tb.vhd
--  9/25/17
--  Braeden Morrison
--
--  A testbench for the portion of the datapath around the memory controller.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity mem_datapath_tb is
    generic (runner_cfg : string);
end mem_datapath_tb;

architecture test of mem_datapath_tb is

    --=========================================================================
    ---- Defined Constants ----------------------------------------------------

    constant CLK_PERIOD :       time := 50 ns;      -- Clock Period
    constant WIDTH :            natural := 32;      -- Width of the datapath

    --=========================================================================
    ---- Component Declarations -----------------------------------------------

    component mem_datapath
        generic (
            --=================================================================
            -- WIDTH:   The word size for the processor (32 bits).
            --=================================================================
            WIDTH :     natural := 32
        );
        port (
            --=================================================================
            ---- Input Signals ----

            -- data_in:     The data that goes to memory during a write. 
            --                  Connects to the rs2 output of the register 
            --                  file.
            -- addr_in:     The address that will be read from or written to. 
            --                  Comes from the output of the ALU.
            --
            -- mem_rw:      Controls whether we read from or write to memory. 
            --                  Comes from the control unit.
            -- mem_bytes:   Determines how many bytes to read or write to 
            --                  memory. Comes from the control unit.
            -- sign_ext:    Determines whether the output is zero or sign 
            --                  extended. Comes from the control unit.
            --
            -- clk:         A one-bit clock signal that gets sent to the PC. 
            --                  Comes from the clock-generating circuitry.
    
            ---- Output Signals ----

            -- mem_out:     The output from the memory unit. Goes to the ALU
            --                  potion of the datapath.
            --=================================================================

            data_in :       in  std_logic_vector(WIDTH-1 downto 0);
            addr_in :       in  std_logic_vector(WIDTH-1 downto 0);
            mem_rw :        in  std_logic;
            mem_bytes :     in  std_logic_vector(1 downto 0);
            sign_ext :      in  std_logic;
            clk :           in  std_logic;

            mem_out :       out std_logic_vector(WIDTH-1 downto 0)
        );
    end component mem_datapath;

    --=========================================================================
    ---- Test Signals ---------------------------------------------------------

    ---- Input Signals ----

    signal data_in :        std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');
    signal addr_in :        std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');

    signal mem_rw :         std_logic := '0';
    signal mem_bytes :      std_logic_vector(1 downto 0)
                                                := (others => '0');
    signal sign_ext :       std_logic := '0';

    signal clk :            std_logic := '0';
    
    ---- Output Signals ----

    signal mem_out :        std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');

begin

    --=========================================================================
    ---- Component Instances --------------------------------------------------

    UUT : mem_datapath
        port map (
            -- Inputs --
            data_in     => data_in,
            addr_in     => addr_in,

            mem_rw      => mem_rw,
            mem_bytes   => mem_bytes,
            sign_ext    => sign_ext,

            clk         => clk,

            -- Outputs --
            mem_out     => mem_out
        );

    --=========================================================================
    ---- Test Processes -------------------------------------------------------

    -- Process that controls the clock
    clk_proc : process
    begin
        clk <= NOT clk;
        wait for CLK_PERIOD/2;
    end process;

    -- Process that runs some test cases on the datapath
    stim_proc : process
    begin

        test_runner_setup(runner, runner_cfg);  -- Start Vunit

        -- Attempt memory write word --
        mem_bytes <= "11"; 
        mem_rw <= '1';
        data_in <= X"1234_5678";
        addr_in <= X"0000_0000";
        wait for CLK_PERIOD;

        -- Attempt memory write hword --
        mem_bytes <= "10";
        mem_rw <= '1';
        data_in <= X"9999_FACE"; -- The '9's should be ignored
        addr_in <= X"0000_0004";
        wait for CLK_PERIOD;

        -- Attempt memory write byte --
        mem_bytes <= "01";
        mem_rw <= '1';
        data_in <= X"9999_99AD";
        addr_in <= X"0000_0006";
        wait for CLK_PERIOD;

        -- Write another byte so that we have a clean 8 bytes --
        mem_bytes <= "01";
        mem_rw <= '1';
        data_in <= X"9999_99DE";
        addr_in <= X"0000_0007";
        wait for CLK_PERIOD;

        -- Test memory read word --
        mem_bytes <= "11";
        mem_rw <= '0';
        addr_in <= X"0000_0004";
        wait for CLK_PERIOD;

        assert (mem_out = X"DEAD_FACE")
            report "Incorrect value obtained from LW!"
            severity error;

        -- Test memory read unsigned hword --
        mem_bytes <= "10";
        mem_rw <= '0';
        sign_ext <= '0';
        addr_in <= X"0000_0006";
        wait for CLK_PERIOD;

        assert (mem_out = X"0000_DEAD")
            report "Incorrect value obtained from LHU!"
            severity error;

        -- Test memory read signed negative hword --
        mem_bytes <= "10";
        mem_rw <= '1';
        sign_ext <= '1';
        addr_in <= X"0000_0004";
        wait for CLK_PERIOD;

        assert (mem_out = X"FFFF_FACE")
            report "Incorrect value from LH on a negative number!"
            severity error;

        -- Test memory read signed positive hword --
        mem_bytes <= "10";
        mem_rw <= '1';
        sign_ext <= '1';
        addr_in <= X"0000_0002";
        wait for CLK_PERIOD;

        assert (mem_out = X"0000_1234")
            report "Incorrect value from LH on a positive number!"
            severity error;

        -- Test memory read unsigned byte --
        mem_bytes <= "01";
        mem_rw <= '0';
        sign_ext <= '0';
        addr_in <= X"0000_0003";
        wait for CLK_PERIOD;

        assert (mem_out = X"0000_0012")
            report "Incorrect value obtained from LBU!"
            severity error;

        -- Test memory read signed negative byte --
        mem_bytes <= "01";
        mem_rw <= '1';
        sign_ext <= '1';
        addr_in <= X"0000_0005";
        wait for CLK_PERIOD;

        assert (mem_out = X"FFFF_FFFA")
            report "Incorrect value from LB on a negative number!"
            severity error;

        -- Test memory read signed positive byte --
        mem_bytes <= "01";
        mem_rw <= '1';
        sign_ext <= '1';
        addr_in <= X"0000_0001";
        wait for CLK_PERIOD;

        assert (mem_out = X"0000_0056")
            report "Incorrect value from LB on a positive number!"
            severity error;

        test_runner_cleanup(runner);    -- Stop testbench
    end process;

end test;
