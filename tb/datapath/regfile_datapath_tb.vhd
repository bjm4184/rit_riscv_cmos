--  regfile_datapath_tb.vhd
--  9/27/17
--  Braeden Morrison
--
--  A testbench for the portion of the datapath around the register file.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity regfile_datapath_tb is
    generic (runner_cfg : string);
end regfile_datapath_tb;

architecture test of regfile_datapath_tb is

    --=========================================================================
    ---- Defined Constants ----------------------------------------------------

    constant CLK_PERIOD :       time := 50 ns;      -- Clock Period
    constant WIDTH :            natural := 32;      -- Width of the datapath
    constant SEL_BITS :         natural := 5;       -- log2(WIDTH)
    constant AUIPC_IMM_BITS :   natural := 20;
    constant LUI_IMM_BITS :     natural := 20;

    --=========================================================================
    ---- Component Declarations -----------------------------------------------

    component regfile_datapath
        generic (
            --=================================================================
            -- WIDTH:           The word size for the processor (32 bits).
            -- SEL_BITS:        Number of bits needed to specify a register.
            -- AUIPC_IMM_BITS:  Size of the immediate for an AUIPC.
            -- LUI_IMM_BITS:    Size of the immediate for a LUI.
            --=================================================================
            WIDTH :             natural := 32;
            SEL_BITS :          natural := 5;
            AUIPC_IMM_BITS :    natural := 20;
            LUI_IMM_BITS :      natural := 20
        );
        port (
            --=================================================================
            ---- Input Signals ----

            -- pc_in:       The value output by the PC. Used in AUIPC, JAL, and
            --                  JALR instructions. Comes from the PC datapath.
            -- alu_in:      The result of the ALU operation. Comes from the ALU
            --                  datapath.
            -- mem_in:      The output from the memory controller; representing
            --                  a value read from memory. Comes from the mem
            --                  datapath.
            --
            -- auipc_imm:   The unshifted immediate value that will eventually
            --                  be added to the PC in a AUIPC instruction.
            --                  comes from the instruction decode.
            -- lui_imm:     The unshifted immediate value that will be used in
            --                  an LUI instruction. Comes from the instruction
            --                  decode.
            --
            -- rwrite_en:   Controls whether or not a value is written to the
            --                  register file. Comes from the control unit.
            -- reg_in_ctrl: Determines which value will be stored into the 
            --                  register file. Comes from the control unit.
            -- res_ctrl:    Determines whether the output from the ALU or from
            --                  memory will go to the register file. Comes from
            --                  the control unit.
            --
            -- clk:         A one-bit clock signal that gets sent to the 
            --                  register file. Comes from the clock-generating
            --                  circuitry.
            -- rs1_sel:     The signal that selects which register will be used
            --                  as rs1. Comes from the control unit.
            -- rs2_sel:     The signal that selects which register will be used
            --                  as rs2. Comes from the control unit.
            -- rd_sel:      The signal that selects which register will be used
            --                  as rd. Comes from the control unit.
    
            ---- Output Signals ----

            -- rs1_out:     The output from the first source register. Goes
            --                  to the PC and ALU datapaths.
            -- rs2_out:     The output from the second source register. Goes
            --                  to the mem and ALU datapaths.
            --=================================================================

            pc_in :         in  std_logic_vector(WIDTH-1 downto 0);
            alu_in :        in  std_logic_vector(WIDTH-1 downto 0);
            mem_in :        in  std_logic_vector(WIDTH-1 downto 0);
            auipc_imm :     in  std_logic_vector(AUIPC_IMM_BITS-1 downto 0);
            lui_imm :       in  std_logic_vector(LUI_IMM_BITS-1 downto 0);
            rwrite_en :     in  std_logic;
            reg_in_ctrl :   in  std_logic_vector(1 downto 0);
            res_ctrl :      in  std_logic;
            clk :           in  std_logic;
            rs1_sel :       in  std_logic_vector(SEL_BITS-1 downto 0);
            rs2_sel :       in  std_logic_vector(SEL_BITS-1 downto 0);
            rd_sel :        in  std_logic_vector(SEL_BITS-1 downto 0);

            rs1_out :       out std_logic_vector(WIDTH-1 downto 0);
            rs2_out :       out std_logic_vector(WIDTH-1 downto 0)
        );
    end component regfile_datapath;

    --=========================================================================
    ---- Test Signals ---------------------------------------------------------

    ---- Input Signals ----

    signal pc_in :          std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');
    signal alu_in :         std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');
    signal mem_in :         std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');

    signal auipc_imm :      std_logic_vector(AUIPC_IMM_BITS-1 downto 0)
                                                := (others => '0');
    signal lui_imm :        std_logic_vector(LUI_IMM_BITS-1 downto 0)
                                                := (others => '0');

    signal rwrite_en :      std_logic := '0';
    signal reg_in_ctrl :    std_logic_vector(1 downto 0) := "00";
    signal res_ctrl :       std_logic := '0';
    signal clk :            std_logic := '0';

    signal rs1_sel :        std_logic_vector(SEL_BITS-1 downto 0)
                                                := (others => '0');
    signal rs2_sel :        std_logic_vector(SEL_BITS-1 downto 0)
                                                := (others => '0');
    signal rd_sel :         std_logic_vector(SEL_BITS-1 downto 0)
                                                := (others => '0');

    ---- Output Signals ----

    signal rs1_out :        std_logic_vector(WIDTH-1 downto 0);
    signal rs2_out :        std_logic_vector(WIDTH-1 downto 0);

begin

    --=========================================================================
    ---- Component Instances --------------------------------------------------

    UUT : regfile_datapath
        port map (
            -- Inputs --
            pc_in       => pc_in,
            alu_in      => alu_in,
            mem_in      => mem_in,

            auipc_imm   => auipc_imm,
            lui_imm     => lui_imm,

            rwrite_en   => rwrite_en,
            reg_in_ctrl => reg_in_ctrl,
            res_ctrl    => res_ctrl,

            clk         => clk,

            rs1_sel     => rs1_sel,
            rs2_sel     => rs2_sel,
            rd_sel      => rd_sel,

            -- Outputs --
            rs1_out     => rs1_out,
            rs2_out     => rs2_out
        );

    --=========================================================================
    ---- Test Processes -------------------------------------------------------

    -- Process that controls the clock
    clk_proc : process
    begin
        clk <= NOT clk;
        wait for CLK_PERIOD/2;
    end process;

    -- Process that runs some test cases on the datapath
    stim_proc : process
    begin

        test_runner_setup(runner, runner_cfg);  -- Start Vunit

        ---- Store and read a value from the memory input ----
        mem_in <= X"1234_4321";
        rwrite_en <= '1';       -- enable writing to the register file
        res_ctrl <= '0';        -- choose memory input over alu input
        reg_in_ctrl <= "00";    -- choose memory/alu over other inputs
        rd_sel <= "00001";      -- store the value in register 1.
        wait for CLK_PERIOD;

        rs1_sel <= "00001";     -- Read the value that was just stored.
        wait for CLK_PERIOD;

        assert (rs1_out = X"1234_4321")
            report "mem_in store/read test failed!"
            severity error;

        ---- Store and read a value from the ALU input ----
        alu_in <= X"FEDC_BA98";
        rwrite_en <= '1';       -- enable writing to the register file
        res_ctrl <= '1';        -- choose alu input over memory input
        reg_in_ctrl <= "00";    -- choose memory/alu over other inputs
        rd_sel <= "00010";      -- store the value in register 2.
        wait for CLK_PERIOD;

        rs2_sel <= "00010";     -- Read the value that was just stored.
        wait for CLK_PERIOD;

        assert (rs2_out = X"FEDC_BA98")
            report "alu_in store/read test failed!"
            severity error;

        ---- Test a LUI instruction ----
        lui_imm <= X"B_0B0B";
        rwrite_en <= '1';       -- enable writing to the register file
        reg_in_ctrl <= "01";    -- choose LUI input over the others
        rd_sel <= "00011";      -- store the value in register 3.
        wait for CLK_PERIOD;

        rs1_sel <= "00011";     -- Read the value that was just stored.
        wait for CLK_PERIOD;

        assert (rs1_out = X"B0B0_B000")
            report "LUI test failed!"
            severity error;

        ---- Test a AUIPC instruction ----
        pc_in <= X"0000_BABE";
        auipc_imm <= X"B_EEF0";
        rwrite_en <= '1';       -- enable writing to the register file
        reg_in_ctrl <= "10";    -- choose AUIPC input over the others
        rd_sel <= "00100";      -- store the value in register 4.
        wait for CLK_PERIOD;

        rs2_sel <= "00100";     -- Read the value that was just stored.
        wait for CLK_PERIOD;

        assert (rs2_out = X"BEEF_BABE")
            report "AUIPC test failed!"
            severity error;

        ---- Test a JAL instruction  and the rwrite_en signal ----
        pc_in <= X"0123_4567";
        rwrite_en <= '1';        -- enable writing to the register file
        reg_in_ctrl <= "11";    -- choose JAL input over the others
        rd_sel <= "00101";      -- store the value in register 5.
        wait for CLK_PERIOD;

        rs1_sel <= "00101";
        wait for CLK_PERIOD;

        assert (rs1_out = X"0123_4567")
            report "JAL test failed!"
            severity error;

        pc_in <= X"7654_3210";
        rwrite_en <= '0';       -- disable writing to the register file
        wait for CLK_PERIOD;

        assert (rs1_out = X"0123_4567") -- the output shouldn't change
            report "rwrite_en test failed!"
            severity error;
        
        test_runner_cleanup(runner);    -- Stop testbench
    end process;

end test;
