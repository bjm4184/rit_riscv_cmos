--  mem_controller_tb.vhd
--  10/9/17
--  Braeden Morrison
--
--  A testbench for the memory controller.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity mem_controller_tb is
    generic (runner_cfg : string);
end mem_controller_tb;

architecture test of mem_controller_tb is

    --=========================================================================
    ---- Defined Constants ----------------------------------------------------

    constant CLK_PERIOD :       time := 50 ns;      -- Clock Period
    constant WIDTH :            natural := 32;      -- Width of the datapath
    constant MEM_SIZE :         natural := 512;     -- The number of bytes that
                                                    -- are actually memory.

    --=========================================================================
    ---- Component Declarations -----------------------------------------------

    component mem_controller
        generic (
            --=================================================================
            -- WIDTH:   The word size for the processor (32 bits).
            --=================================================================
            WIDTH :     natural := 32
        );
        port (
            --=================================================================
            ---- Input Signals ----

            -- data_in:     The data that goes to memory during a write. 
            --                  Connects to the rs2 output of the register 
            --                  file.
            -- addr_in:     The address that will be read from or written to. 
            --                  Comes from the output of the ALU.
            --
            -- mem_rw:      Controls whether we read from or write to memory. 
            --                  Comes from the control unit.
            -- mem_bytes:   Determines how many bytes to read or write to 
            --                  memory. Comes from the control unit.
            --
            -- clk:         A one-bit clock signal that gets sent to the PC. 
            --                  Comes from the clock-generating circuitry.
    
            ---- Output Signals ----

            -- word_out:    A 32-bit output from the memory used when reading
            --                  full words. Goes to the datapath.
            -- hword_out:   A 16-bit output from the memory used when reading
            --                  half words. Goes to the datapath.
            -- byte_out:    A 8-bit output from the memory used when reading
            --                  individual bytes. Goes to the datapath.
            --=================================================================

            data_in :       in  std_logic_vector(WIDTH-1 downto 0);
            addr_in :       in  std_logic_vector(WIDTH-1 downto 0);
            mem_rw :        in  std_logic;
            mem_bytes :     in  std_logic_vector(1 downto 0);
            clk :           in  std_logic;

            word_out :      out std_logic_vector(WIDTH-1 downto 0);
            hword_out :     out std_logic_vector((WIDTH/2)-1 downto 0);
            byte_out :      out std_logic_vector(7 downto 0)
        );
    end component mem_controller;

    --=========================================================================
    ---- Test Signals ---------------------------------------------------------

    ---- Input Signals ----

    signal data_in :        std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');
    signal addr_in :        std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');

    signal mem_rw :         std_logic := '0';
    signal mem_bytes :      std_logic_vector(1 downto 0)
                                                := (others => '0');

    signal clk :            std_logic := '0';
    
    ---- Output Signals ----

    signal word_out :       std_logic_vector(WIDTH-1 downto 0)
                                                := (others => '0');
    signal hword_out :      std_logic_vector((WIDTH/2)-1 downto 0)
                                                := (others => '0');
    signal byte_out :       std_logic_vector(7 downto 0)
                                                := (others => '0');

begin

    --=========================================================================
    ---- Component Instances --------------------------------------------------

    UUT : mem_controller
        port map (
            -- Inputs --
            data_in     => data_in,
            addr_in     => addr_in,

            mem_rw      => mem_rw,
            mem_bytes   => mem_bytes,

            clk         => clk,

            -- Outputs --
            word_out    => word_out,
            hword_out   => hword_out,
            byte_out    => byte_out
        );

    --=========================================================================
    ---- Test Processes -------------------------------------------------------

    -- Process that controls the clock
    clk_proc : process
    begin
        clk <= NOT clk;
        wait for CLK_PERIOD/2;
    end process;

    -- Process that runs some test cases on the datapath
    stim_proc : process
    begin

        test_runner_setup(runner, runner_cfg);  -- Start Vunit

        -- BJM[NOTE]: The test coverage for this moduel is a little light, but
        --              I don't think that's an issue since this module is
        --              going to change a lot in the future and this version
        --              is mostly just acting as a place-holder.

        ------------------------------
        ---- Word read/write test ----

        -- Store word in memory
        mem_rw      <= '1';     -- store
        mem_bytes   <= "11";    -- full word
        data_in     <= X"89AB_CDEF";
        addr_in     <= X"0000_0000";
        wait for CLK_PERIOD;

        -- Now read it back
        mem_rw      <= '0';     -- read
        wait for CLK_PERIOD;

        -- Check the result
        assert (word_out = X"89AB_CDEF")
            report "Invalid results for word read/write test"
            severity error;

        -----------------------------------
        ---- Half-Word read/write test ----

        -- Store hword in memory
        mem_rw      <= '1';     -- store
        mem_bytes   <= "10";    -- half-word
        data_in     <= X"0000_CAFE";
        addr_in     <= X"0000_0042";
        wait for CLK_PERIOD;

        -- Now read it back
        mem_rw      <= '0';     -- read
        wait for CLK_PERIOD;

        -- Check the result
        assert (hword_out = X"CAFE")
            report "Invalid results for half-word read/write test"
            severity error;

        -----------------------------------
        ---- Byte read/write test ----

        -- Store hword in memory
        mem_rw      <= '1';     -- store
        mem_bytes   <= "01";    -- byte
        data_in     <= X"0000_0051";
        addr_in     <= X"0000_00EF";
        wait for CLK_PERIOD;

        -- Now read it back
        mem_rw      <= '0';     -- read
        wait for CLK_PERIOD;

        -- Check the result
        assert (byte_out = X"51")
            report "Invalid results for byte read/write test"
            severity error;

        -----------------------------------
        ---- Null read/write test ----

        -- Store hword in memory
        mem_rw      <= '1';     -- store
        mem_bytes   <= "00";    -- no write
        data_in     <= X"FFFF_FFFF";
        addr_in     <= X"0000_00EF";
        wait for CLK_PERIOD;

        -- Now read it back
        mem_rw      <= '0';     -- read
        wait for CLK_PERIOD;

        -- Check the result
        assert (byte_out = X"51")
            report "Memory changed despite mem_bytes being '00'"
            severity error;

        test_runner_cleanup(runner);    -- Stop testbench
    end process;

end test;
