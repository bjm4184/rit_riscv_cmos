-- pkgConstants.vhd
-- Andrew Eberhard
-- 10/15/17
--
-- Package defining the necessary constants needed for ALU architectures and-- testbenches.
--==========================================================================
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

PACKAGE pkgConstants IS

    --======================================================================
    -- Defined Constants --
    CONSTANT    PERIOD      :TIME       :=  20  ns;
    CONSTANT    WIDTH       :NATURAL    :=  32;
    CONSTANT    OP_WIDTH    :NATURAL    :=  03;

END pkgConstants;
